alembic==1.3.2
backcall==0.1.0
Click==7.0
colorama==0.4.3
decorator==4.4.1
Flask==1.1.1
Flask-Migrate==2.5.2
Flask-SQLAlchemy==2.4.1
GeoAlchemy2==0.6.3
gunicorn==20.0.4
ipykernel==5.1.3
ipython==7.11.1
ipython-genutils==0.2.0
itsdangerous==1.1.0
jedi==0.15.2
Jinja2==2.10.3
jupyter-client==5.3.4
jupyter-core==4.6.1
Mako==1.1.0
MarkupSafe==1.1.1
numpy==1.17.4
pandas==0.25.3
parso==0.5.2
pickleshare==0.7.5
pip-licenses==2.1.0
prompt-toolkit==3.0.2
psycopg2==2.8.4
PTable==0.9.2
Pygments==2.5.2
python-dateutil==2.8.1
python-editor==1.0.4
pytz==2019.3
PyYAML==5.2
pyzmq==18.1.1
six==1.13.0
SQLAlchemy==1.3.12
tornado==6.0.3
traitlets==4.3.3
wcwidth==0.1.8
Werkzeug==0.16.0
