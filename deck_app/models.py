from app import db, engine


schema_gtfs = 'gtfs'
schema_crash = 'crash'
schema_od_trip = 'od_trip'
schema_bluetooth = 'bluetooth_traveltime'
schema_bikes = 'bike_counts'


# GTFS ---------- ---------- ---------- ---------- ---------- ----------
class GTFSAgency(db.Model):
    __tablename__  = 'agency'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    agency_name = db.Column(db.String(128))
    agency_url = db.Column(db.Text)
    agency_timezone = db.Column(db.Text)
    agency_lang = db.Column(db.String(128))
    agency_phone = db.Column(db.String(128))

    def __repr__(self):
        return 'agency_name %r' % (self.agency_name)


class GTFSCalendar(db.Model):
    __tablename__  = 'calendar'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    service_id = db.Column(db.String(128))
    monday = db.Column(db.Integer)
    tuesday = db.Column(db.Integer)
    wednesday = db.Column(db.Integer)
    thursday = db.Column(db.Integer)
    friday = db.Column(db.Integer)
    saturday = db.Column(db.Integer)
    sunday = db.Column(db.Integer)
    start_date = db.Column(db.String(128))
    end_date = db.Column(db.String(128))

    def __repr__(self):
        return 'service_id %r' % (self.service_id)


class GTFSCalendarDates(db.Model):
    __tablename__  = 'calendar_dates'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    service_id = db.Column(db.String(128))
    date = db.Column(db.String(128))
    exception_type = db.Column(db.Integer)

    def __repr__(self):
        return 'service_id %r' % (self.service_id)


class GTFSFeedInfo(db.Model):
    __tablename__  = 'feed_info'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    feed_publisher_name = db.Column(db.String(128))
    feed_publisher_url = db.Column(db.Text)
    feed_lang = db.Column(db.String(128))
    feed_start_date = db.Column(db.String(128))
    feed_end_date = db.Column(db.String(128))

    def __repr__(self):
        return 'feed_publisher_name %r' % (self.feed_publisher_name)


class GTFSRoutes(db.Model):
    __tablename__  = 'routes'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    route_id = db.Column(db.String(128))
    route_short_name = db.Column(db.String)
    route_long_name = db.Column(db.String)
    route_desc = db.Column(db.Text)
    route_type = db.Column(db.Integer)
    route_url = db.Column(db.Text)
    route_color = db.Column(db.String(128))
    route_text_color = db.Column(db.String(128))

    def __repr__(self):
        return 'route_id %r' % (self.route_id)


class GTFSShapes(db.Model):
    __tablename__  = 'shapes'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    shape_id = db.Column(db.String)
    shape_pt_lat = db.Column(db.Float)
    shape_pt_lon  = db.Column(db.Float)
    shape_pt_sequence = db.Column(db.Integer)

    def __repr__(self):
        return 'shape_id %r' % (self.shape_id)


class GTFSStops(db.Model):
    __tablename__  = 'stops'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    stop_id = db.Column(db.String)
    stop_code = db.Column(db.String)
    stop_name = db.Column(db.String(128))
    stop_desc = db.Column(db.Text)
    stop_lat = db.Column(db.Float)
    stop_lon = db.Column(db.Float)
    zone_id = db.Column(db.String)
    stop_url = db.Column(db.Text)
    location_type = db.Column(db.Integer)
    parent_station = db.Column(db.String(128))
    platform_code = db.Column(db.String(128))

    def __repr__(self):
        return 'stop_id %r' % (self.stop_id)


class GTFSStopTimes(db.Model):
    __tablename__  = 'stop_times'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    trip_id = db.Column(db.String(128))
    arrival_time = db.Column(db.String(128))
    departure_time = db.Column(db.String(128))
    stop_id = db.Column(db.Integer)
    stop_sequence = db.Column(db.Integer)
    pickup_type = db.Column(db.Integer)
    drop_off_type = db.Column(db.Integer)

    def __repr__(self):
        return 'stop_id %r' % (self.trip_id)


class GTFSTrips(db.Model):
    __tablename__  = 'trips'
    __table_args__ = {'schema' : schema_gtfs}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    route_id = db.Column(db.String(128))
    service_id = db.Column(db.String(128))
    trip_id = db.Column(db.String(128))
    trip_headsign = db.Column(db.String(128))
    direction_id = db.Column(db.Integer)
    block_id = db.Column(db.String)
    shape_id = db.Column(db.String)

    def __repr__(self):
        return 'trip_id %r' % (self.route_id)

# ---------- ---------- ---------- ---------- ---------- ---------- ----------

# crash ---------- ---------- ---------- ---------- ---------- ----------
class CrashDriverDemograpgics(db.Model):
    __tablename__  = 'driver_demographics'
    __table_args__ = {'schema' : schema_crash}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    crash_year = db.Column(db.Integer)
    crash_police_region = db.Column(db.String)
    crash_severity = db.Column(db.String)
    involving_male_driver = db.Column(db.String)
    involving_female_driver = db.Column(db.String)
    involving_young_driver_16_24 = db.Column(db.String)
    involving_senior_driver_60plus = db.Column(db.String)
    involving_provisional_driver = db.Column(db.String)
    involving_overseas_licensed_driver = db.Column(db.String)
    involving_unlicensed_driver = db.Column(db.String)
    count_crashes = db.Column(db.Integer)
    count_casualty_fatality = db.Column(db.Integer)
    count_casualty_hospitalised = db.Column(db.Integer)
    count_casualty_medicallytreated = db.Column(db.Integer)
    count_casualty_minorinjury = db.Column(db.Integer)
    count_casualty_all =  db.Column(db.Integer)

    def __repr__(self):
        return '<id %r>' % (self.id)


class FactorsInRoadCrashes(db.Model):
    __tablename__  = 'factors_in_road_crashes'
    __table_args__ = {'schema' : schema_crash}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    crash_year = db.Column(db.Integer)
    crash_police_region = db.Column(db.String)
    crash_severity = db.Column(db.String)
    involving_drink_driving = db.Column(db.String)
    involving_driver_speed = db.Column(db.String)
    involving_fatigued_driver = db.Column(db.String)
    involving_defective_vehicle	= db.Column(db.String)
    count_crashes = db.Column(db.Integer)
    count_fatality = db.Column(db.Integer)
    count_hospitalised = db.Column(db.Integer)
    count_medically_treated = db.Column(db.Integer)
    count_minor_injury = db.Column(db.Integer)
    count_all_casualties = db.Column(db.Integer)

    def __repr__(self):
        return '<factor %r>' % (self.id)


class locations(db.Model):
    __tablename__  = 'locations'
    __table_args__ = {'schema' : schema_crash}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    crash_ref_number = db.Column(db.Integer)
    crash_severity = db.Column(db.String)
    crash_year = db.Column(db.Integer)
    crash_month = db.Column(db.String)
    crash_day_of_week = db.Column(db.String)
    crash_hour = db.Column(db.Integer)
    crash_nature = db.Column(db.String)
    crash_type = db.Column(db.String)
    crash_longitude_gda94 = db.Column(db.Float)
    crash_latitude_gda94 = db.Column(db.Float)
    crash_street = db.Column(db.String)
    crash_street_intersecting = db.Column(db.String)
    state_road_name = db.Column(db.String)
    loc_suburb = db.Column(db.String)
    loc_local_government_area = db.Column(db.String)
    loc_post_code = db.Column(db.String)
    loc_police_division = db.Column(db.String)
    loc_police_district = db.Column(db.String)
    loc_police_region = db.Column(db.String)
    loc_queensland_transport_region = db.Column(db.String)
    loc_main_roads_region = db.Column(db.String)
    loc_abs_statistical_area_2 = db.Column(db.String)
    loc_abs_statistical_area_3 = db.Column(db.String)
    loc_abs_statistical_area_4 = db.Column(db.String)
    loc_abs_remoteness = db.Column(db.String)
    loc_state_electorate = db.Column(db.String)
    loc_federal_electorate = db.Column(db.String)
    crash_controlling_authority = db.Column(db.String)
    crash_roadway_feature = db.Column(db.String)
    crash_traffic_control = db.Column(db.String)
    crash_speed_limit = db.Column(db.String)
    crash_road_surface_condition = db.Column(db.String)
    crash_atmospheric_condition = db.Column(db.String)
    crash_lighting_condition = db.Column(db.String)
    crash_road_horiz_align = db.Column(db.String)
    crash_road_vert_align = db.Column(db.String)
    crash_dca_code = db.Column(db.Integer)
    crash_dca_description = db.Column(db.String)
    crash_dca_group_description = db.Column(db.String)
    dca_key_approach_dir = db.Column(db.String)
    count_casualty_fatality = db.Column(db.Integer)
    count_casualty_hospitalised = db.Column(db.Integer)
    count_casualty_medicallytreated = db.Column(db.Integer)
    count_casualty_minorinjury = db.Column(db.Integer)
    count_casualty_total = db.Column(db.Integer)
    count_unit_car = db.Column(db.Integer)
    count_unit_motorcycle_moped = db.Column(db.Integer)
    count_unit_truck = db.Column(db.Integer)
    count_unit_bus = db.Column(db.Integer)
    count_unit_bicycle = db.Column(db.Integer)
    count_unit_pedestrian = db.Column(db.Integer)
    count_unit_other = db.Column(db.Integer)

    def __repr__(self):
        return '<location %r>' % (self.crash_ref_number)


class RestraintHelmetUse(db.Model):
    __tablename__  = 'restraint_helmet_use'
    __table_args__ = {'schema' : schema_crash}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    crash_year = db.Column(db.Integer)
    crash_policeregion = db.Column(db.String)
    casualty_severity = db.Column(db.String)
    casualty_agegroup = db.Column(db.String)
    casualty_gender = db.Column(db.String)
    casualty_road_user_type = db.Column(db.String)
    casualty_restraint_helmet_use = db.Column(db.String)
    casualty_count = db.Column(db.Integer)

    def __repr__(self):
        return '<restraint_helmet_use %r>' % (self.id)


class VehicleInvolvement(db.Model):
    __tablename__  = 'vehicle_involvement'
    __table_args__ = {'schema' : schema_crash}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    crash_year = db.Column(db.Integer)
    crash_police_region = db.Column(db.String)
    crash_severity = db.Column(db.String)
    involving_motorcycle_moped = db.Column(db.String)
    involving_truck = db.Column(db.String)
    involving_bus = db.Column(db.String)
    count_crashes = db.Column(db.Integer)
    count_casualty_fatality = db.Column(db.Integer)
    count_casualty_hospitalised = db.Column(db.Integer)
    count_casualty_medicallytreated = db.Column(db.Integer)
    count_casualty_minorinjury = db.Column(db.Integer)
    count_casualty_all = db.Column(db.Integer)

    def __repr__(self):
        return '<vehicle_involvement %r>' % (self.id)

# ---------- ---------- ---------- ---------- ---------- ---------- ----------

# OD Trip ---------- ---------- ---------- ---------- ---------- ----------
class PTODTrip(db.Model):
    __tablename__ = 'od_trips'
    __table_args__ = {'schema': schema_od_trip}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    operator = db.Column(db.String)
    month = db.Column(db.String)
    route = db.Column(db.String)
    direction = db.Column(db.String)
    time = db.Column(db.String)
    ticket_type = db.Column(db.String)
    origin_stop = db.Column(db.String)
    destination_stop = db.Column(db.String)
    quantity = db.Column(db.Integer)

    def __repr__(self):
        return '<OD Trip %r>' % (self.id) 

# ---------- ---------- ---------- ---------- ---------- ---------- ----------

# Bluetooth Travel Time ---------- ---------- ---------- ---------- ----------
class BTLink(db.Model):
    __tablename__ = 'links'
    __table_args__ = {'schema': schema_bluetooth}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    link_details = db.Column(db.String)
    origin_desc = db.Column(db.Text)
    origin_longitude = db.Column(db.Float)
    origin_latitude = db.Column(db.Float)
    dest_desc = db.Column(db.Text)
    dest_longitude = db.Column(db.Float)
    dest_latitude = db.Column(db.Float)

    def __repr__(self):
        return '<BT Link %r>' % (self.link_detail)


class BTTravelTime(db.Model):
    __tablename__ = 'travel_times'
    __table_args__ = {'schema': schema_bluetooth}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    interval_end = db.Column(db.String)
    od_1098_1056 = db.Column(db.Float)
    od_1058_1059 = db.Column(db.Float)
    od_1057_1056 = db.Column(db.Float)
    od_1017_1007 = db.Column(db.Float)
    od_1115_1015 = db.Column(db.Float)
    od_1015_1115 = db.Column(db.Float)
    od_1103_1061 = db.Column(db.Float)
    od_1135_1231 = db.Column(db.Float)
    od_1260_1261 = db.Column(db.Float)
    od_1072_1061 = db.Column(db.Float)
    od_1070_1069 = db.Column(db.Float)
    od_1122_1123 = db.Column(db.Float)
    od_1124_1133 = db.Column(db.Float)
    od_1137_1136 = db.Column(db.Float)
    od_1127_1089 = db.Column(db.Float)
    od_1085_1086 = db.Column(db.Float)
    od_1088_1089 = db.Column(db.Float)
    od_1172_1171 = db.Column(db.Float)
    od_1165_1532 = db.Column(db.Float)
    od_1544_1539 = db.Column(db.Float)
    od_1539_1537 = db.Column(db.Float)
    od_1075_1076 = db.Column(db.Float)
    od_1078_1077 = db.Column(db.Float)
    od_1076_1075 = db.Column(db.Float)
    od_1182_1515 = db.Column(db.Float)
    od_1090_1573 = db.Column(db.Float)
    od_1573_1538 = db.Column(db.Float)
    od_1095_1096 = db.Column(db.Float)
    od_1006_1081 = db.Column(db.Float)
    od_1080_1130 = db.Column(db.Float)
    od_1156_1170 = db.Column(db.Float)
    od_1059_1060 = db.Column(db.Float)
    od_1058_1057 = db.Column(db.Float)
    od_1160_1154 = db.Column(db.Float)
    od_1091_1092 = db.Column(db.Float)
    od_1126_1120 = db.Column(db.Float)
    od_1124_1123 = db.Column(db.Float)
    od_1140_1138 = db.Column(db.Float)
    od_1089_1127 = db.Column(db.Float)
    od_1127_1229 = db.Column(db.Float)
    od_1059_1171 = db.Column(db.Float)
    od_1184_1173 = db.Column(db.Float)
    od_1173_1174 = db.Column(db.Float)
    od_1543_1529 = db.Column(db.Float)
    od_1532_1527 = db.Column(db.Float)
    od_1534_1544 = db.Column(db.Float)
    od_1076_1077 = db.Column(db.Float)
    od_1062_1107 = db.Column(db.Float)
    od_1573_1090 = db.Column(db.Float)
    od_1067_1096 = db.Column(db.Float)
    od_1095_1133 = db.Column(db.Float)
    od_1172_1170 = db.Column(db.Float)
    od_1097_1098 = db.Column(db.Float)
    od_1059_1058 = db.Column(db.Float)
    od_1056_1098 = db.Column(db.Float)
    od_1007_1115 = db.Column(db.Float)
    od_1162_1154 = db.Column(db.Float)
    od_1239_1231 = db.Column(db.Float)
    od_1129_1260 = db.Column(db.Float)
    od_1132_1092 = db.Column(db.Float)
    od_1092_1091 = db.Column(db.Float)
    od_1069_1070 = db.Column(db.Float)
    od_1070_1097 = db.Column(db.Float)
    od_1099_1061 = db.Column(db.Float)
    od_1061_1072 = db.Column(db.Float)
    od_1140_1271 = db.Column(db.Float)
    od_1088_1087 = db.Column(db.Float)
    od_1087_1088 = db.Column(db.Float)
    od_1270_1071 = db.Column(db.Float)
    od_1158_1546 = db.Column(db.Float)
    od_1539_1544 = db.Column(db.Float)
    od_1063_1064 = db.Column(db.Float)
    od_1538_1573 = db.Column(db.Float)
    od_1096_1095 = db.Column(db.Float)
    od_1170_1156 = db.Column(db.Float)
    od_1081_1006 = db.Column(db.Float)
    od_1084_1083 = db.Column(db.Float)
    od_1005_1002 = db.Column(db.Float)
    od_1002_1006 = db.Column(db.Float)
    od_1055_1097 = db.Column(db.Float)
    od_1057_1058 = db.Column(db.Float)
    od_1103_1162 = db.Column(db.Float)
    od_1154_1160 = db.Column(db.Float)
    od_1161_1160 = db.Column(db.Float)
    od_1189_1178 = db.Column(db.Float)
    od_1134_1142 = db.Column(db.Float)
    od_1261_1129 = db.Column(db.Float)
    od_1131_1129 = db.Column(db.Float)
    od_1097_1070 = db.Column(db.Float)
    od_1128_1126 = db.Column(db.Float)
    od_1084_1090 = db.Column(db.Float)
    od_1090_1084 = db.Column(db.Float)
    od_1527_1533 = db.Column(db.Float)
    od_1157_1528 = db.Column(db.Float)
    od_1544_1534 = db.Column(db.Float)
    od_1063_1062 = db.Column(db.Float)
    od_1156_1068 = db.Column(db.Float)
    od_1006_1002 = db.Column(db.Float)
    od_1005_1083 = db.Column(db.Float)
    od_1081_1080 = db.Column(db.Float)
    od_1170_1172 = db.Column(db.Float)
    od_1060_1025 = db.Column(db.Float)
    od_1134_1178 = db.Column(db.Float)
    od_1125_1131 = db.Column(db.Float)
    od_1094_1093 = db.Column(db.Float)
    od_1132_1093 = db.Column(db.Float)
    od_1094_1164 = db.Column(db.Float)
    od_1099_1055 = db.Column(db.Float)
    od_1123_1124 = db.Column(db.Float)
    od_1229_1127 = db.Column(db.Float)
    od_1087_1086 = db.Column(db.Float)
    od_1084_1085 = db.Column(db.Float)
    od_1171_1172 = db.Column(db.Float)
    od_1530_1523 = db.Column(db.Float)
    od_1533_1523 = db.Column(db.Float)
    od_1523_1530 = db.Column(db.Float)
    od_1546_1158 = db.Column(db.Float)
    od_1064_1075 = db.Column(db.Float)
    od_1064_1063 = db.Column(db.Float)
    od_1181_1515 = db.Column(db.Float)
    od_1130_1080 = db.Column(db.Float)
    od_1056_1057 = db.Column(db.Float)
    od_1060_1059 = db.Column(db.Float)
    od_1097_1055 = db.Column(db.Float)
    od_1015_1025 = db.Column(db.Float)
    od_1061_1103 = db.Column(db.Float)
    od_1142_1134 = db.Column(db.Float)
    od_1178_1189 = db.Column(db.Float)
    od_1231_1239 = db.Column(db.Float)
    od_1131_1125 = db.Column(db.Float)
    od_1093_1132 = db.Column(db.Float)
    od_1069_1068 = db.Column(db.Float)
    od_1121_1122 = db.Column(db.Float)
    od_1133_1124 = db.Column(db.Float)
    od_1123_1122 = db.Column(db.Float)
    od_1120_1126 = db.Column(db.Float)
    od_1126_1128 = db.Column(db.Float)
    od_1171_1059 = db.Column(db.Float)
    od_1529_1530 = db.Column(db.Float)
    od_1532_1165 = db.Column(db.Float)
    od_1075_1064 = db.Column(db.Float)
    od_1515_1181 = db.Column(db.Float)
    od_1174_1181 = db.Column(db.Float)
    od_1133_1095 = db.Column(db.Float)
    od_1096_1067 = db.Column(db.Float)
    od_1067_1068 = db.Column(db.Float)
    od_1068_1156 = db.Column(db.Float)
    od_1025_1060 = db.Column(db.Float)
    od_1098_1097 = db.Column(db.Float)
    od_1025_1015 = db.Column(db.Float)
    od_1115_1007 = db.Column(db.Float)
    od_1007_1017 = db.Column(db.Float)
    od_1160_1161 = db.Column(db.Float)
    od_1154_1162 = db.Column(db.Float)
    od_1162_1103 = db.Column(db.Float)
    od_1125_1142 = db.Column(db.Float)
    od_1189_1135 = db.Column(db.Float)
    od_1135_1189 = db.Column(db.Float)
    od_1074_1072 = db.Column(db.Float)
    od_1061_1099 = db.Column(db.Float)
    od_1137_1128 = db.Column(db.Float)
    od_1120_1121 = db.Column(db.Float)
    od_1121_1120 = db.Column(db.Float)
    od_1128_1137 = db.Column(db.Float)
    od_1271_1136 = db.Column(db.Float)
    od_1271_1140 = db.Column(db.Float)
    od_1089_1088 = db.Column(db.Float)
    od_1086_1085 = db.Column(db.Float)
    od_1086_1087 = db.Column(db.Float)
    od_1155_1071 = db.Column(db.Float)
    od_1082_1006 = db.Column(db.Float)
    od_1082_1270 = db.Column(db.Float)
    od_1174_1173 = db.Column(db.Float)
    od_1533_1527 = db.Column(db.Float)
    od_1527_1532 = db.Column(db.Float)
    od_1529_1543 = db.Column(db.Float)
    od_1543_1546 = db.Column(db.Float)
    od_1528_1534 = db.Column(db.Float)
    od_1537_1539 = db.Column(db.Float)
    od_1181_1174 = db.Column(db.Float)
    od_1515_1182 = db.Column(db.Float)
    od_1068_1067 = db.Column(db.Float)
    od_1002_1005 = db.Column(db.Float)
    od_1083_1005 = db.Column(db.Float)
    od_1231_1135 = db.Column(db.Float)
    od_1178_1134 = db.Column(db.Float)
    od_1142_1125 = db.Column(db.Float)
    od_1129_1131 = db.Column(db.Float)
    od_1164_1094 = db.Column(db.Float)
    od_1092_1132 = db.Column(db.Float)
    od_1093_1094 = db.Column(db.Float)
    od_1068_1069 = db.Column(db.Float)
    od_1055_1099 = db.Column(db.Float)
    od_1072_1074 = db.Column(db.Float)
    od_1136_1137 = db.Column(db.Float)
    od_1122_1121 = db.Column(db.Float)
    od_1138_1140 = db.Column(db.Float)
    od_1136_1271 = db.Column(db.Float)
    od_1085_1084 = db.Column(db.Float)
    od_1071_1270 = db.Column(db.Float)
    od_1270_1082 = db.Column(db.Float)
    od_1006_1082 = db.Column(db.Float)
    od_1071_1155 = db.Column(db.Float)
    od_1172_1184 = db.Column(db.Float)
    od_1173_1184 = db.Column(db.Float)
    od_1184_1172 = db.Column(db.Float)
    od_1546_1543 = db.Column(db.Float)
    od_1523_1180 = db.Column(db.Float)
    od_1180_1533 = db.Column(db.Float)
    od_1530_1529 = db.Column(db.Float)
    od_1534_1528 = db.Column(db.Float)
    od_1528_1157 = db.Column(db.Float)
    od_1107_1062 = db.Column(db.Float)
    od_1062_1063 = db.Column(db.Float)
    od_1077_1078 = db.Column(db.Float)
    od_1077_1076 = db.Column(db.Float)
    od_1080_1081 = db.Column(db.Float)
    od_1083_1084 = db.Column(db.Float)

    def __repr__(self):
        return '<BT Travel Time %r>' % (self.id)

# ---------- ---------- ---------- ---------- ---------- ---------- ----------

# Bike Count ---------- ---------- ---------- ---------- ---------- ----------

class FieldDescription(db.Model):
    __tablename__ = 'field_description'
    __table_args__ = {'schema': schema_bikes}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    field_name = db.Column(db.String)
    description = db.Column(db.Text)

    def __repr__(self):
        return '<Field Description %r>' % (self.field_name)


class CoountSite(db.Model):
    __tablename__ = 'count_site'
    __table_args__ = {'schema': schema_bikes}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    site_id = db.Column(db.Integer)
    description = db.Column(db.Text)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)

    def __repr__(self):
        return '<Count Site %r>' % (self.site_id)


class BikeCount(db.Model):
    __tablename__ = 'bike_count'
    __table_args__ = {'schema': schema_bikes}
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    site_id = db.Column(db.Integer)
    description = db.Column(db.Text)
    months = db.Column(db.String)
    years = db.Column(db.Integer)
    hours = db.Column(db.Integer)
    saturday = db.Column(db.Integer)
    sunday = db.Column(db.Integer)
    monday = db.Column(db.Integer)
    tuesday = db.Column(db.Integer)
    wednesday = db.Column(db.Integer)
    thursday = db.Column(db.Integer)
    friday = db.Column(db.Integer)

    def __repr__(self):
        return '<Bike Count %r>' % (self.site_id)

# ---------- ---------- ---------- ---------- ---------- ---------- ----------


# execute creation of table schema
def init_db():
    """
    Initialize database tables with the specified schema.
    """
    engine.execute("CREATE SCHEMA IF NOT EXISTS " + schema_gtfs)
    engine.execute("CREATE SCHEMA IF NOT EXISTS " + schema_crash)
    engine.execute("CREATE SCHEMA IF NOT EXISTS " + schema_od_trip)
    engine.execute("CREATE SCHEMA IF NOT EXISTS " + schema_bluetooth)
    engine.execute("CREATE SCHEMA IF NOT EXISTS " + schema_bikes)
    db.create_all()


def init_data():
    """
    Initialize database tables by 
    firstly deleting table data if exists, 
    secondly reseting primary key sequences, 
    then adding data from raw data files to tables.
    """

    # create a connection and transactions, then commit
    with engine.begin() as connection: 

        # Delete all existing data if exists
        delete_data_sql = [
            'delete_gtfs.sql',
            'delete_crash.sql',
            'delete_odtrip.sql',
            'delete_bluetooth.sql',
            'delete_bikes.sql'
            ]
        for sql_file in delete_data_sql:
            with open('./sql/' + sql_file,'r') as sql_queries:
                for sql_line in sql_queries.readlines():
                    try:
                        print('Executing SQL: ', sql_line, '.....')
                        connection.execute(sql_line)
                    except:
                        connection.close()
                        raise

        # Reset primary key sequence
        reset_data_sql = [
            'reset_gtfs.sql',
            'reset_crash.sql',
            'reset_odtrip.sql',
            'reset_bluetooth.sql',
            'reset_bikes.sql'
            ]
        for sql_file in reset_data_sql:
            with open('./sql/' + sql_file,'r') as sql_queries:
                for sql_line in sql_queries.readlines():
                    try:
                        print('Executing SQL: ', sql_line, '.....')
                        connection.execute(sql_line)
                    except:
                        connection.close()
                        raise

        # Add data to tables from raw data files
        copy_data_sql = [
            'copy_gtfs.sql',
            'copy_crash.sql',
            'copy_odtrip.sql',
            'copy_bluetooth.sql',
            'copy_bikes.sql'
            ]
        for sql_file in copy_data_sql:
            with open('./sql/' + sql_file,'r') as sql_queries:
                for sql_line in sql_queries.readlines():
                    try:
                        print('Executing SQL: ', sql_line, '.....')
                        connection.execute(sql_line)
                    except:
                        connection.close()
                        raise