from sql.sql_engine import sql_dict
from app import engine

import pandas as pd
import json
from datetime import datetime


########## Data Load ########## ########## ########## ########## ########## ##########

def query_data(sql):
    return pd.read_sql(sql, con=engine)


########## Transformation ########## ########## ########## ########## ########## ##########

def jsonify_scatter_data(data, data_coords):
    """ convert dataframe to json for deck.gl scatter layer format"""
    
    dict_data = data.to_dict()
    dict_coords = {'coordinates': data_coords.values.tolist()}
    dict_data.update(dict_coords)

    return dict_data


def jsonify_hexagon_data(data, data_coords):
    """ convert dataframe to json for deck.gl hexagon layer format"""
    
    dict_data = data.to_dict()
    dict_coords = {'COORDINATES': data_coords.values.tolist()}
    dict_data.update(dict_coords)

    return dict_data


def jsonify_column_data(data, data_centroids):

    dict_data = data.to_dict()
    dict_centroid = {'COORDINATES': data_centroids.values.tolist()}
    dict_data.update(dict_centroid)

    return dict_data


def jsonify_line_data(data, data_from, data_to):

    json_data = data.to_dict()
    json_from = {'from': {'coordinates': data_from.values.tolist()}}
    json_to = {'to': {'coordinates': data_to.values.tolist()}}

    json_data.update(json_from)
    json_data.update(json_to)

    return json_data


def transform_bluetooth_data(df_traveltime):
    
    result = df_traveltime.melt(
        id_vars=['id','interval_end'], 
        var_name='link_from_to',
        value_name='travel_time'
        )
    result['link_from_to'] = result['link_from_to'].str.replace('od_','').str.replace('_','->')
    # format od_1098_1056 to match 1060->1025

    return result


########## Layer Creation ########## ########## ########## ########## ########## ##########    

def create_layer_scatter(sql, cols_coords):
    df = query_data(sql)
    
    scatter_data = df.apply(
        lambda x: jsonify_scatter_data(
            x.loc[~df.columns.isin(cols_coords)], 
            x.loc[cols_coords]
            ), axis=1
        ).values.tolist()
        
    return scatter_data
    

def create_layer_column(sql, cols_centroids):
    df = query_data(sql)

    column_data = df.apply(
        lambda x: jsonify_column_data(
            x.loc[~df.columns.isin(cols_centroids)], 
            x.loc[cols_centroids]
            ), axis=1
        ).values.tolist()
    
    return column_data


def create_layer_line(sql, cols_from, cols_to):
    df = query_data(sql)

    line_data = df.apply(
        lambda x: jsonify_line_data(
            x.loc[~df.columns.isin(cols_from + cols_to)], 
            x.loc[cols_from],
            x.loc[cols_to]
            ), axis=1
        ).values.tolist()
    
    return line_data


def create_layer_traveltime(sql_links, sql_traveltime, cols_from, cols_to):
    """
    Create a dict style data for bluetooth travel time for the createion of deck.gl layer.
    
    Args:
        sql_links: A SQL statement to extract bluetooth link data.
        sql_traveltime: A SQL statement to extract travel time data.
        cols_from: A list of strings column names that contains longitude and latitude for the line start point. 
        cols_to: A list of strings column names that contains longitude and latitude for the line end point.

    Returns:
        line_data: A dict data contains bluetooth travel time link details and attributes. For example:

        {
            'id': 1, 
            'link_from_to': '1060->1025', 
            'interval_end': '1/10/2019 0:00',
            'travel_time': 100.0
            'origin_desc': 'Intersection of Gympie and Beams Roads (M1132)', 
            'origin_longitude': 153.012445, 
            'origin_latitude': -27.345904, 
            'dest_desc': '200m South of the South Pine River Bridge (CS802)',
            'dest_longitude': 153.083721, 
            'dest_latitude': -27.368208
        }
    """

    # extract bluetooth sites and travel time data
    df_links = query_data(sql_links)
    df_traveltime = query_data(sql_traveltime)
    
    # transform travel time dataframe
    df_traveltime = transform_bluetooth_data(df_traveltime)
    
    # merge bluetooth link and travel time dataframes
    df = pd.merge(
        df_links, 
        df_traveltime[['link_from_to','interval_end','travel_time']], 
        how='inner', 
        left_on='link_details', 
        right_on='link_from_to'
        ).drop('link_details', axis=1)
        
    # create line layer from the merged dataframe
    line_data = df.apply(
        lambda x: jsonify_line_data(
            x.loc[~df.columns.isin(cols_from + cols_to)], 
            x.loc[cols_from],
            x.loc[cols_to]
            ), axis=1
        ).values.tolist()
    
    return line_data


def create_layer_od_trips(route, year_month, day_time):
    """
    Create 3 datasets (od_trips, stops, route) for 3 deck.gl layers (Arc, Scatterplot, Path).

    Overall Procedure
    1. Define initial route_id='100'
    2. Get od_trip, gtfs_stop, gtfs_routes datasets
    3. Get all OD trips data for the route
    4. Get all stops involving the route
    5. Get the shape for the route as wkt
    6. Convert OD Trip to Arc and Stop to Scatterplot

    Args:

    Returns:
    od_trips_data: A list of dict data for od trips for arc layer. Example:
    [
        {
            'id': 20580,
            'operator': 'Clarks Logan City Bus Service',
            'month': '2019-04',
            'route': '555',
            'direction': 'Inbound',
            'time': 'Weekend',
            'ticket_type': 'go card',
            'origin_stop': '10802',
            'destination_stop': '10814',
            'quantity': 1,
            'from': {'coorinates': [153.019415, -27.473232]},
            'to': {'coorinates': [153.039185, -27.497023]}
        }
    ]

    stops_data: A list of dict data route stops for scatterplot layer. Example:
    [
        {'stop_id': '1024', 'coordinates': [-27.627806, 152.966365]},
        {'stop_id': '1026', 'coordinates': [-27.627967, 152.965641]},
        {'stop_id': '10386', 'coordinates': [-27.525557, 153.022598]}
    ]

    shapes_data: A list of dict data route shapes for path layer. Example:
    [
        {
            'id': 8931,
            'shape_id': '1000056',
            'shape_pt_sequence': 10001,
            'path': [152.969936, -27.623727]
        }
    ]
    """

    route_id = route
    year_month = year_month
    day_time = day_time

    # Extract od_trips data with input arguments
    df = pd.read_sql(
        f"SELECT * FROM od_trip.od_trips WHERE ticket_type='go card' AND month='{ year_month }' AND time='{ day_time }'", 
        con=engine
        )

    # Extract all pt stop data with lat lon
    df_stops = pd.read_sql("SELECT stop_id, stop_lat, stop_lon FROM gtfs.stops", con=engine)
    
    # Get od trips data for a route 
    def get_od_trips_for_route(route_id):

        # Get all relevant od trip data and stop latitude and longitude
        od_data = df[
            df['route']==route_id
        ].merge(df_stops, how='inner', left_on='origin_stop', right_on='stop_id').rename(columns={
            'stop_lat': 'origin_lat', 'stop_lon': 'origin_lon'
        }).merge(df_stops, how='inner', left_on='destination_stop', right_on='stop_id').rename(columns={
            'stop_lat': 'destination_lat', 'stop_lon': 'destination_lon'
        }).drop(['stop_id_x','stop_id_y'], axis=1)
        
        # Create df for boarding and alighting data
        boardings = od_data.groupby(
            ['month','time','direction','origin_stop'], as_index=False
        )['quantity'].sum()
        
        alightings = od_data.groupby(
            ['month','time','direction','destination_stop'], as_index=False
        )['quantity'].sum()

        # Create from and to columns for coordinates, then drop latitude and longitude columns
        od_data['from'] = od_data[['origin_lon','origin_lat']].values.tolist()
        od_data['to'] = od_data[['destination_lon','destination_lat']].values.tolist()
        od_data = od_data.drop(['origin_lon','origin_lat'] + ['destination_lon','destination_lat'], axis=1)

        # Convert from and to columns to dict format with the key 'coordinates'
        od_data['from'] = od_data['from'].apply(lambda orig: {'coordinates': orig})
        od_data['to'] = od_data['to'].apply(lambda dest: {'coordinates': dest})

        # Convert dataframe to dict
        od_data = od_data.to_dict(orient='records')


        return od_data, boardings, alightings


    # get the shape for the route
    def get_shape_for_route(route_id, group_cols, lon, lat):

        # sql to get gtfs routes data for input route_id
        sql_get_route_id = f"""
                            SELECT 
                                route_id, 
                                route_short_name, 
                                route_long_name
                            FROM gtfs.routes WHERE route_short_name = '{route_id}'
                            """

        # extract gtfs route data
        route_df = pd.read_sql(sql_get_route_id, con=engine)
        list_route_id = "('" + "','".join([r for r in route_df['route_id'].unique().tolist()]) + "')"

        # sql to get gtfs shape_id, direction_id and trip_id for input route_id
        sql_get_shape_id = f"""
                            SELECT DISTINCT ON (shape_id, direction_id) route_id, shape_id, direction_id, trip_id
                            FROM gtfs.trips 
                            WHERE route_id IN {list_route_id} AND trip_id LIKE '%%Weekday%%'
                            """

        # extract gtfs shape_id data
        shape_df = pd.read_sql(sql_get_shape_id, con=engine)
        shape_df = shape_df[~shape_df['trip_id'].str.contains('HUN')] # TO BE CONFIRMED IF THIS WORKS!!
        list_shape_id = "('" + "','".join([s for s in shape_df['shape_id'].unique().tolist()]) + "')"

        # sql to get gtfs shapes data for input route_id
        sql_get_shape = f"""SELECT * FROM gtfs.shapes WHERE shape_id IN {list_shape_id}"""

        # extract gtfs shape_id latitude longitude
        shapes_df_all_dir = pd.read_sql(sql_get_shape, con=engine)

        # get the max length of shape_id, direction_id, trip_id
        shape_lengths_df = shapes_df_all_dir.groupby(['shape_id'], as_index=False)['id'].count()
        df_shapes = pd.merge(shape_df, shape_lengths_df, how='inner', on='shape_id')
        df_shapes_longest = df_shapes.groupby(['direction_id'], as_index=False)[['id','shape_id','trip_id']].max()

        # longest shape of the service
        df_shapes_latlon = shapes_df_all_dir[
            (shapes_df_all_dir['shape_id'].isin(df_shapes_longest['shape_id']))
        ]

        # list of trip_id to be used to get stop sequence
        list_trip_id = "('" + "','".join([s for s in df_shapes_longest['trip_id'].unique().tolist()]) + "')"

        # convert dataframe with latitude longitude to WKT Linestring
        # sort shape dataframe by pt stop sequence
        df_shapes_latlon['shape_id'] = df_shapes_latlon['shape_id'].astype(str)
        df_shapes_latlon['shape_pt_sequence'] = df_shapes_latlon['shape_pt_sequence'].astype(int)
        
        df = df_shapes_latlon.sort_values(['shape_id', 'shape_pt_sequence'])
        
        # create a column with 'longitude latitude'
        # for wkt format
        #df.loc[:, 'lonlat'] = df[lon].astype(str).str[:] + ' ' + df[lat].astype(str).str[:]
        # for deck.gl path layer format and drop lat lon columns
        df['path'] = df[[lon, lat]].values.tolist()
        df = df.drop([lon, lat], axis=1)

        # groupby group_cols and aggregate point geometry
        # to linestring in wkt format
        #df_linestring = df.groupby(group_cols, as_index=False).agg({'lonlat': lambda point: 'LINESTRING ' + '(' + ', '.join([p for p in point]) + ')'})
        # to path in deck.gl path layer format
        df_linestring = df.groupby(group_cols, as_index=False).agg({'path': lambda point: [p for p in point]})
        df_linestring = df_linestring.to_dict(orient='records')


        return df_linestring, list_trip_id


    # Get all of the stops along a route
    def get_all_stops_for_route(route_id, list_trip_id):
        
        # SQL to extract the stops and stop sequence
        sql_stop_times = f"""
                        SELECT 
                            st.trip_id,
                            st.stop_id,
                            st.stop_sequence,
                            s.stop_name,
                            s.stop_lat,
                            s.stop_lon
                        FROM gtfs.stop_times st
                        INNER JOIN gtfs.stops s ON s.stop_id::text = st.stop_id::text
                        WHERE trip_id IN { list_trip_id }
                        """

        route_stops = pd.read_sql(sql_stop_times, con=engine)
        stops_df = route_stops.copy()
        
        # Create a column for coordinates
        route_stops = route_stops.sort_values(['trip_id','stop_sequence'])
        route_stops['coordinates'] = route_stops[['stop_lon','stop_lat']].values.tolist()
        route_stops = route_stops.drop(['stop_lon','stop_lat'], axis=1)

        # Convert dataframe to dict
        route_stops = route_stops.to_dict(orient='records')


        return route_stops, stops_df


    def summarise_boarding_alighting(data_board, data_alight, stops):
        
        # create a copy of stop df to prepare for merging
        stops_data = stops.copy()
        stops_data['stop_id'] = stops_data['stop_id'].astype(str)
        
        # merge stops with boarding and alighting data
        boarding_data_stops = pd.merge(data_board, stops_data, how='left', left_on='origin_stop', right_on='stop_id')
        alighting_data_stops = pd.merge(data_alight, stops_data, how='left', left_on='destination_stop', right_on='stop_id')
        
        # define aggregation functions
        agg_f_boarding = {
            'trip_id': lambda x: x.iloc[0],
            'month': lambda x: x.iloc[0],
            'time': lambda x: x.iloc[0],
            'origin_stop': lambda x: x.iloc[0],
            'stop_name': lambda x: x.iloc[0],
            'quantity': sum,
            'stop_lat': lambda x: x.iloc[0],
            'stop_lon': lambda x: x.iloc[0]
        }
        
        agg_f_alighting = {
            'trip_id': lambda x: x.iloc[0],
            'month': lambda x: x.iloc[0],
            'time': lambda x: x.iloc[0],
            'destination_stop': lambda x: x.iloc[0],
            'stop_name': lambda x: x.iloc[0],
            'quantity': sum,
            'stop_lat': lambda x: x.iloc[0],
            'stop_lon': lambda x: x.iloc[0]
        }
        
        # summarise boarding and alighting data so same stop locations are aggregated by direction (trip_id)
        boarding_data_stops = boarding_data_stops.groupby(['direction','stop_sequence'],as_index=False).agg(agg_f_boarding)
        alighting_data_stops = alighting_data_stops.groupby(['direction','stop_sequence'],as_index=False).agg(agg_f_alighting)

        # add trip_type column to indicate 'boarding' or 'alighting'
        boarding_data_stops.loc[:,'trip_type'] = 'boarding'
        alighting_data_stops.loc[:,'trip_type'] = 'alighting'
        
        # sort df by trip_id and stop_sequence
        boarding_data_stops = boarding_data_stops.sort_values(['direction','stop_sequence'])
        alighting_data_stops = alighting_data_stops.sort_values(['direction','stop_sequence'])
        
        # concat boarding and alighting df to single df
        board_alight_data = pd.concat(
            [
                boarding_data_stops.rename(columns={'origin_stop':'stop_id'}), 
                alighting_data_stops.rename(columns={'destination_stop':'stop_id'})
            ], join='inner', axis=0
        ).to_dict(orient='records')
        
        return board_alight_data


    od_trips_data, boarding_data, alighting_data = get_od_trips_for_route(route_id)
    shapes_data, list_trip_id = get_shape_for_route(route_id, ['shape_id'], 'shape_pt_lon', 'shape_pt_lat')
    stops_data, stops_df = get_all_stops_for_route(route_id, list_trip_id)
    board_alight_data = summarise_boarding_alighting(boarding_data, alighting_data, stops_df)

    return od_trips_data, board_alight_data, stops_data, shapes_data


def create_layer_crash(sql, cols_coords):
    """
    Create crash data for deck.gl hexagon layer data

    Args:
        sql: A sql statement to extract data.
        cols_coords: A list of column names used for hexagon coordinates.

    Returns:
        df_crash_locations: A list containing dictionaries of crash data for hexagon layer. Example:
        
        [
            {
                'crash_ref_number': 11654,
                'crash_severity': 'Hospitalisation',
                'crash_year': 2005,
                'crash_month': 'October',
                'crash_day_of_week': 'Tuesday',
                'crash_hour': 14,
                'crash_nature': 'Rear-end',
                'crash_type': 'Multi-Vehicle',
                'COORDINATES': [153.080852852, -27.540025835]
            }
        ]
    """

    df_crash_locations = query_data(sql)

    # Check unique values of crash ref number ID
    if len(df_crash_locations['crash_ref_number']) == len(df_crash_locations['crash_ref_number'].unique()):
        print('crash_ref_number', '\n', len(df_crash_locations['crash_ref_number']), '\n')
        
    else:
        print('Duplicate IDs in: ', 'crash_ref_number')
        
 
    # Display unique values: crash_severity
    print('crash_severity', '\n', df_crash_locations['crash_severity'].unique(), '\n')

    # Display unique values: crash_year
    print('crash_year', '\n', df_crash_locations['crash_year'].sort_values().unique(), '\n')

    # Display unique values: crash_month
    print('crash_month', '\n', df_crash_locations['crash_month'].sort_values().unique(), '\n')

    # Display unique values: crash_day_of_week
    print('crash_day_of_week', '\n', df_crash_locations['crash_day_of_week'].unique(), '\n')

    # Display unique values: crash_hour
    print('crash_hour', '\n', df_crash_locations['crash_hour'].sort_values().unique(), '\n')

    # Display unique values: crash_nature
    print('crash_nature', '\n', df_crash_locations['crash_nature'].sort_values().unique(), '\n')

    # Display unique values: crash_type
    print('crash_type', '\n', df_crash_locations['crash_type'].sort_values().unique(), '\n')

    # Set spatial limit
    west = 137.936807
    east = 153.738545
    north = -9.944126
    south = -29.240209

    # Get data with longitude outside QLD (not precisely)
    error_longitude = df_crash_locations[
        (df_crash_locations['crash_longitude_gda94'] < west) | 
        (df_crash_locations['crash_longitude_gda94'] > east)
    ]['crash_ref_number'].values.tolist()

    # Get data with latitude outside QLD (not precisely)
    error_latitude = df_crash_locations[
        (df_crash_locations['crash_latitude_gda94'] > north) |
        (df_crash_locations['crash_latitude_gda94'] < south)
    ]['crash_ref_number'].values.tolist()

    # List data outside QLD
    error_coordinates = pd.Series(error_longitude + error_latitude).drop_duplicates().tolist()

    # Filter out crash data located outside QLD 
    df_crash_locations = df_crash_locations[~df_crash_locations['crash_ref_number'].isin(error_coordinates)]

    # Create a coordinates column based on cols_coords input
    df_crash_locations['COORDINATES'] = df_crash_locations[cols_coords].values.tolist()

    # Drop latitude and longitude columns
    df_crash_locations = df_crash_locations.drop(cols_coords, axis=1)

    # Create hexagon layer data by converting the dataframe to dict
    df_crash_locations = df_crash_locations.to_dict(orient='records')

    print('Completed data processing')

    return df_crash_locations


