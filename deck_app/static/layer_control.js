// Function to change layer visibility
function toggle_visibility(element_id) {
    let visibility = map.getLayoutProperty(element_id, 'visibility');

    // turn-off layer if visible
    if (visibility == 'visible') {
        map.setLayoutProperty(element_id, 'visibility', 'none');
    }
    // turn-on layer if invisible
    else {
        map.setLayoutProperty(element_id, 'visibility', 'visible');
    }
};