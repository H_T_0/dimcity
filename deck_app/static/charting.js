// Function to manipulate data and creating s chart

// Bike Counts
function bike_count_charts(total_data, profile_data) {

    // Create daily total chart
    BIKE_DAILY_TOTAL = document.getElementById('chart-bike-daily-total'); // DOM for the bike count total chart

    Plotly.newPlot(
        BIKE_DAILY_TOTAL, 
        total_data, 
        {
            height: 250,
            margin: { l: 40, t: 30, b: 50, pad: 5 },
            font: { color: 'rgb(230, 230, 230)' },
            paper_bgcolor: 'rgb(100, 100, 100)',
            plot_bgcolor: 'rgb(100, 100, 100)',
            title: 'Daily Counts',
            xaxis: { showgrid: false, zeroline: false, showline: false },
            yaxis: { showgrid: false, zeroline: false, showline: false },
            hovermode: 'closest'
        },
      )

    // Create daily profile chart
    BIKE_DAILY_PROFILE = document.getElementById('chart-bike-daily-profile'); // DOM for the bike count profile chart

    Plotly.newPlot(
        BIKE_DAILY_PROFILE, profile_data, 
        {
            height: 250,
            margin: { l: 40, t: 30, b: 40, pad: 5 },
            font: { color: 'rgb(230, 230, 230)' },
            paper_bgcolor: 'rgb(100, 100, 100)',
            plot_bgcolor: 'rgb(100, 100, 100)',
            title: 'Daily Count Profile',
            xaxis: { showgrid: false, zeroline: false, showline: false, title: 'Hour' },
            yaxis: { showgrid: false, zeroline: false, showline: false },
            hovermode: 'closest'
        }
    )
}


// Bike Counts - Function to update charts with animation
function bike_count_charts_update(total_data, profile_data) {

    // Update daily total chart with new data
    BIKE_DAILY_TOTAL = document.getElementById('chart-bike-daily-total');
    input_data = total_data
    // Get min and max of new data
    ymin = Math.min(...total_data[0]['y']);
    ymax = Math.max(...total_data[0]['y']);

    // Update the chart with animation by Plotly.animate( DOM, {data, layout}, {animationAttributes} )
    Plotly.animate( 
        BIKE_DAILY_TOTAL, 
        {
            data: total_data, 
            layout: {
                yaxis: { range: [ymin, ymax] }
            }
        }, 
        {
            transition: {
                duration: 500,
                easing: 'cubic-in-out'
            },
            frame: {
                  duration: 500
              }
        }
    )


    // Update daily profile chart
    BIKE_DAILY_PROFILE = document.getElementById('chart-bike-daily-profile');

    // Get min and max of new data
    min_array = [];
    max_array = [];
    for (p_data of profile_data) {
        min_array.push(Math.min(...p_data['y']));
        max_array.push(Math.max(...p_data['y']));
    }
    ymin = Math.min(...min_array);
    ymax = Math.max(...max_array);

    // Update the chart with animation by Plotly.animate( DOM, {data, layout}, {animationAttributes} )
    Plotly.animate(
        BIKE_DAILY_PROFILE, 
        {
            data: profile_data, 
            layout: {
                yaxis: { range: [ymin, ymax] }
            }
        },
        {
            transition: {
                duration: 500,
                easing: 'cubic-in-out'
            },
            frame: {
                  duration: 500
              }
        }
    )
}



// OD Trips
function od_trips_charts(boarding_alighting_data) {
    console.log('CHART DATA:', boarding_alighting_data)

    // if data has only one direction
    if (boarding_alighting_data.length == 1) {
        let trip_dir0 = boarding_alighting_data[0];
        let trip_dir0_board_data = trip_dir0.board_data;
        let trip_dir0_alight_data = trip_dir0.alight_data;
        let trip_dir0_stops_data = trip_dir0.stop_data;

        // Get the max y value to set yaxis range
        let all_y_values = [];
        for (dir_data of boarding_alighting_data) {
            for (b_val of dir_data.board_data.y) {
                all_y_values.push(b_val)
            };
            for (a_val of dir_data.alight_data.y) {
                all_y_values.push(a_val * -1) // bring back alighting val to be positive
            };
        };

        let max_y_val = Math.max(...all_y_values);

        // Get the stops data to update xaxis range
        window.x_dir0 = boarding_alighting_data[0].stop_data.x
        
        // Create boarding and alighting area chart - dir 1
        OD_TRIP_DIR_0 = document.getElementById('chart-od-trips-dir1');

        // Create the chart
        Plotly.newPlot(
            OD_TRIP_DIR_0, 
            [trip_dir0_board_data, trip_dir0_alight_data, trip_dir0_stops_data], 
            {
                autosize: true,
                margin: { l: 40, t: 45, r: 5, b: 20, pad: 5 },
                font: { color: 'rgb(230, 230, 230)', size: 10 },
                paper_bgcolor: 'rgb(100, 100, 100)',
                plot_bgcolor: 'rgb(100, 100, 100)',
                title: 'Boarding and Alighting - ' + trip_dir0.direction,
                xaxis: { visible: false, range: [x_dir1[0] - 0.5, x_dir1.slice(-1)[0] + 0.5] },
                yaxis: { range: [max_y_val * -1, max_y_val], title: 'Trips', side: 'left', zeroline: false },
                hovermode: 'closest',
                legend: { orientation: 'h', x: 0.25, y: 1.1 },
                annotations: [
                    { x: trip_dir0_stops_data.x[0], y: 50, xref: 'x', yref: 'y', text: trip_dir0_stops_data.text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                    { x: trip_dir0_stops_data.x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: trip_dir0_stops_data.text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                ]
            },
            { displayModeBar: false }
        );

        // Add mouse hover event
        add_hover_event(OD_TRIP_DIR_0, 'chart-od-trips-dir1');
    }

    else {
        let trip_dir1 = boarding_alighting_data[0];
        let trip_dir1_board_data = trip_dir1.board_data;
        let trip_dir1_alight_data = trip_dir1.alight_data;
        let trip_dir1_stops_data = trip_dir1.stop_data;

        let trip_dir2 = boarding_alighting_data[1];
        let trip_dir2_board_data = trip_dir2.board_data;
        let trip_dir2_alight_data = trip_dir2.alight_data;
        let trip_dir2_stops_data = trip_dir2.stop_data;

        // Get the max y value to set yaxis range
        let all_y_values = [];
        for (dir_data of boarding_alighting_data) {
            for (b_val of dir_data.board_data.y) {
                all_y_values.push(b_val)
            };
            for (a_val of dir_data['alight_data']['y']) {
                all_y_values.push(a_val * -1) // bring back alighting val to be positive
            };
        };
        
        let max_y_val = Math.max(...all_y_values);

        // Get the stops data to update xaxis range
        window.x_dir1 = boarding_alighting_data[0].stop_data.x
        window.x_dir2 = boarding_alighting_data[1]['stop_data']['x']
        
        // Create boarding and alighting area chart - dir 1
        OD_TRIP_DIR_1 = document.getElementById('chart-od-trips-dir1');

        // Create the chart
        Plotly.newPlot(
            OD_TRIP_DIR_1, 
            [trip_dir1_board_data, trip_dir1_alight_data, trip_dir1_stops_data], 
            {
                autosize: true,
                margin: { l: 40, t: 45, r: 5, b: 20, pad: 5 },
                font: { color: 'rgb(230, 230, 230)', size: 10 },
                paper_bgcolor: 'rgb(100, 100, 100)',
                plot_bgcolor: 'rgb(100, 100, 100)',
                title: 'Boarding and Alighting - ' + trip_dir1.direction,
                xaxis: { visible: false, range: [x_dir1[0] - 0.5, x_dir1.slice(-1)[0] + 0.5] },
                yaxis: { range: [max_y_val * -1, max_y_val], title: 'Trips', side: 'left', zeroline: false },
                hovermode: 'closest',
                legend: { orientation: 'h', x: 0.25, y: 1.1 },
                annotations: [
                    { x: trip_dir1_stops_data.x[0], y: 50, xref: 'x', yref: 'y', text: trip_dir1_stops_data.text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                    { x: trip_dir1_stops_data.x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: trip_dir1_stops_data.text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                ]
            },
            { displayModeBar: false }
        );

        // Add mouse hover event
        add_hover_event(OD_TRIP_DIR_1, 'chart-od-trips-dir1');

        // Create boarding and alighting area chart - dir 2
        OD_TRIP_DIR_2 = document.getElementById('chart-od-trips-dir2');

        Plotly.newPlot(
            OD_TRIP_DIR_2, 
            [trip_dir2_board_data, trip_dir2_alight_data, trip_dir2_stops_data], 
            {
                autosize: true,
                margin: { l: 5, t: 45, r: 40, b: 20, pad: 5 },
                font: { color: 'rgb(230, 230, 230)', size: 10 },
                paper_bgcolor: 'rgb(100, 100, 100)',
                plot_bgcolor: 'rgb(100, 100, 100)',
                title: 'Boarding and Alighting - ' + trip_dir2.direction,
                xaxis: { visible: false, range: [x_dir2[0] - 0.5, x_dir2.slice(-1)[0] + 0.5] },
                yaxis: { range: [max_y_val * -1, max_y_val], title: 'Trips', side: 'right', zeroline: false },
                hovermode: 'closest',
                legend: { orientation: 'h', x: 0.35, y: 1.1 },
                annotations: [
                    { x: trip_dir2_stops_data.x[0], y: 50, xref: 'x', yref: 'y', text: trip_dir2_stops_data.text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                    { x: trip_dir2_stops_data.x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: trip_dir2_stops_data.text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                ]
            },
            { displayModeBar: false }
        );

        // Add mouse hover event
        add_hover_event(OD_TRIP_DIR_2, 'chart-od-trips-dir2');
    }
}


// OD Trips - Function to update charts with animation
function od_trips_charts_update(boarding_alighting_data) {
    console.log('UPDATED BOARDING ALIGHTING DATA:', boarding_alighting_data);
    
    // Get the max y value to set yaxis range
    let all_y_values = [];
    for (dir_data of boarding_alighting_data) {
        for (b_val of dir_data.board_data.y) {
            all_y_values.push(b_val)
        };
        for (a_val of dir_data.alight_data.y) {
            all_y_values.push(a_val * -1) // bring back alighting val to be positive
        };
    };
    
    let max_y_val = Math.max(...all_y_values);
    
    // if data has only one direction
    if (boarding_alighting_data.length == 1) {

        console.log('ONLY ONE DIRECTION OF DATA AVAILABLE')

        // Get the stops data to update xaxis range
        x_dir1 = boarding_alighting_data[0].stop_data.x

        // Update boarding alighting chart dir 1
        OD_TRIP_DIR_1 = document.getElementById('chart-od-trips-dir1');

        // Update the chart with animation by Plotly.animate( DOM, {data, layout}, {animationAttributes} )
        Plotly.animate( 
            OD_TRIP_DIR_1, 
            {
                data: [
                    boarding_alighting_data[0].board_data, 
                    boarding_alighting_data[0].alight_data, 
                    boarding_alighting_data[0].stop_data
                ], 
                layout: {
                    autosize: true,
                    title: 'Boarding and Alighting - ' + boarding_alighting_data[0].direction,
                    xaxis: { range: [x_dir1[0] - 0.5, x_dir1.slice(-1)[0] + 0.5] },
                    yaxis: { range: [max_y_val * -1, max_y_val] },
                    annotations: [
                        { x: boarding_alighting_data[0].stop_data.x[0], y: 50, xref: 'x', yref: 'y', text: boarding_alighting_data[0].stop_data.text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                        { x: boarding_alighting_data[0].stop_data.x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: boarding_alighting_data[0].stop_data.text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                    ]
                }
            }, 
            {
                transition: {duration: 500, easing: 'cubic-in-out'},
                frame: {duration: 500}
            }
        )

        // Update hover event
        add_hover_event(OD_TRIP_DIR_1, 'chart-od-trips-dir1');
    }

    // if data has 2 directions
    else {

        // Get the stops data to update xaxis range
        x_dir1 = boarding_alighting_data[0]['stop_data']['x']
        x_dir2 = boarding_alighting_data[1]['stop_data']['x']

        // Update boarding alighting chart dir 1
        OD_TRIP_DIR_1 = document.getElementById('chart-od-trips-dir1');

        // Update the chart with animation by Plotly.animate( DOM, {data, layout}, {animationAttributes} )
        Plotly.animate( 
            OD_TRIP_DIR_1, 
            {
                data: [
                    boarding_alighting_data[0].board_data, 
                    boarding_alighting_data[0].alight_data, 
                    boarding_alighting_data[0].stop_data
                ], 
                layout: {
                    autosize: true,
                    title: 'Boarding and Alighting - ' + boarding_alighting_data[0].direction,
                    xaxis: { range: [x_dir1[0] - 0.5, x_dir1.slice(-1)[0] + 0.5] },
                    yaxis: { range: [max_y_val * -1, max_y_val] },
                    annotations: [
                        { x: boarding_alighting_data[0]['stop_data'].x[0], y: 50, xref: 'x', yref: 'y', text: boarding_alighting_data[0]['stop_data'].text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                        { x: boarding_alighting_data[0]['stop_data'].x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: boarding_alighting_data[0]['stop_data'].text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                    ]
                }
            }, 
            {
                transition: {duration: 500, easing: 'cubic-in-out'},
                frame: {duration: 500}
            }
        )

        // Update hover event
        add_hover_event(OD_TRIP_DIR_1, 'chart-od-trips-dir1');

        // Update daily profile chart
        OD_TRIP_DIR_2 = document.getElementById('chart-od-trips-dir2');

        // Update the chart with animation by Plotly.animate( DOM, {data, layout}, {animationAttributes} )
        Plotly.animate(
            OD_TRIP_DIR_2, 
            {
                data: [
                    boarding_alighting_data[1]['board_data'], 
                    boarding_alighting_data[1]['alight_data'], 
                    boarding_alighting_data[1]['stop_data']
                ], 
                layout: {
                    autosize: true,
                    title: 'Boarding and Alighting - ' + boarding_alighting_data[1].direction,
                    xaxis: { range: [x_dir2[0] - 0.5, x_dir2.slice(-1)[0] + 0.5] },
                    yaxis: { range: [max_y_val * -1, max_y_val] },
                    annotations: [
                        { x: boarding_alighting_data[1]['stop_data'].x[0], y: 50, xref: 'x', yref: 'y', text: boarding_alighting_data[1]['stop_data'].text[0], showarrow: false, align: 'left', xshift: 50, yshift: 10, width: 100 },
                        { x: boarding_alighting_data[1]['stop_data'].x.slice(-1)[0], y: -50, xref: 'x', yref: 'y', text: boarding_alighting_data[1]['stop_data'].text.slice(-1)[0], showarrow: false, align: 'left', xshift: -50, yshift: -10, width: 100 }
                    ]
                }
            },
            {
                transition: {duration: 500, easing: 'cubic-in-out'},
                frame: {duration: 500}
            }
        )
        
        // Update hover event
        add_hover_event(OD_TRIP_DIR_2, 'chart-od-trips-dir2');
    }    
}


function add_hover_event(dom, dom_id) {
    // Hover event handler - on Hover
    dom.on('plotly_hover', function(hover_data) {
        var pn='',
            tn='',
            colors=[];
            
        for(var i=0; i < hover_data.points.length; i++){
            pn = hover_data.points[i].pointNumber;
            tn = hover_data.points[i].curveNumber;
            // update style ony if the hovered data point us bus_stops (tn = 2)
            if (tn === 2) {
                colors = hover_data.points[i].data.marker.color;
            }
        };

        if (tn === 2) {
            // updating color array for the hovered data point
            colors[pn] = 'rgba(250, 70, 240, 0.9)';
            let update = { 'marker': { color: colors } };

            // apply styling
            Plotly.restyle(dom_id, update, [tn]);
        }
      });

    // Hover event handler - off Hover
    dom.on('plotly_unhover', function(hover_data) {
        var pn='',
            tn='',
            colors=[];
            
        for(var i=0; i < hover_data.points.length; i++){
            pn = hover_data.points[i].pointNumber;
            tn = hover_data.points[i].curveNumber;
            // update style ony if the hovered data point us bus_stops (tn = 2)
            if (tn === 2) {
                colors = hover_data.points[i].data.marker.color;
            }
        };

        if (tn === 2) {
            // updating color array for the hovered data point
            colors[pn] = 'rgba(230, 230, 230, 0.9)';
            let update = { 'marker': { color: colors } };

            // apply styling
            Plotly.restyle(dom_id, update, [tn]);
        }
        });

}