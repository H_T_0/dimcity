//Data controls
//select data filter and update the data in-memory
//then update map and charts accordingly

//jquery for data controls
$(document).ready(function () {

    //Bike Counts - Year
    $(".data-bike-year").change(function() {
        //Get all selected filter values
        let filter_year = $(this).children("option:selected").val();
        let filter_month = $(".data-bike-month").children("option:selected").val();
        let filter_day = $(".data-bike-day").children("option:selected").val();
        sync_update_bike_counts(filter_year, filter_month, filter_day);
    })
    //Bike Counts - Day
    $(".data-bike-month").change(function() {
        //Get all selected filter values
        let filter_year = $(".data-bike-year").children("option:selected").val();
        let filter_month = $(this).children("option:selected").val();
        let filter_day = $(".data-bike-day").children("option:selected").val();
        sync_update_bike_counts(filter_year, filter_month, filter_day);
    })
    //Bike Counts - Day
    $(".data-bike-day").change(function() {
        //Get all selected filter values
        let filter_year = $(".data-bike-year").children("option:selected").val();
        let filter_month = $(".data-bike-month").children("option:selected").val();
        let filter_day = $(this).children("option:selected").val();
        sync_update_bike_counts(filter_year, filter_month, filter_day);
    })


    //OD Trips Data Update - Requires AJAX POST Request as in data_extract.js
    //OD Trips - Route
    $(".data-od-trips-route").change(function() {
        //Get all selected filter values
        let filter_route = $(this).children("option:selected").val();
        let filter_year_month = $(".data-od-trips-year-month").children("option:selected").val();
        let filter_day_time = $(".data-od-trips-day-time").children("option:selected").val();
        sync_update_od_trips(filter_route, filter_year_month, filter_day_time);
    })

    //OD Trips - Year/Month
    $(".data-od-trips-year-month").change(function() {
        //Get all selected filter values
        let filter_route = $(".data-od-trips-route").children("option:selected").val();
        let filter_year_month = $(this).children("option:selected").val();
        let filter_day_time = $(".data-od-trips-day-time").children("option:selected").val();
        sync_update_od_trips(filter_route, filter_year_month, filter_day_time);
    })

    //OD Trips - Day/Time
    $(".data-od-trips-day-time").change(function() {
        //Get all selected filter values
        let filter_route = $(".data-od-trips-route").children("option:selected").val();
        let filter_year_month = $(".data-od-trips-year-month").children("option:selected").val();
        let filter_day_time = $(this).children("option:selected").val();
        sync_update_od_trips(filter_route, filter_year_month, filter_day_time);
    })

})


//Update Data, Map Layers and Charts - Bike Counts
//------------------------------------------------------------------------------------------
function sync_update_bike_counts(filter_year, filter_month, filter_day) {

    console.log('FILTER_VALUES: ', filter_year, filter_month, filter_day);

    //Manipulate data based on filters
    let filtered_data = bike_count_data_manipulation(filter_year, filter_month, filter_day, bike_count_total_data, bike_count_profile_data)
    console.log('FILTERED_MAP_DATA: ', filtered_data['map_data']);
    console.log('FILTERED_TOTAL_DATA: ', filtered_data['total_data']);
    console.log('FILTERED_PROFILE_DATA: ', filtered_data['profile_data']);

    //Update charts
    bike_count_charts_update(filtered_data['total_data'], filtered_data['profile_data']);

    //Update map layer data
    layer_bike_count.setProps({data: filtered_data['map_data'], getRadius: d => d[filter_day]})
}
//------------------------------------------------------------------------------------------


//Process Bike Counts
//------------------------------------------------------------------------------------------
function bike_count_data_manipulation(
    filter_year, 
    filter_month, 
    filter_day, 
    daily_total_data, 
    daily_profile_data
    ) {

    // Functions for data filtering
    function filter_year_month(d) {
        return d['years'] == filter_year && d['months'] == filter_month;
    }

    // Functions for data filtering
    function get_unique_sites(d) {
        let lookup = {};
        let unique_sites = [];

        for (i of d) {
            let site = i['site_id'];
          
            if (!(site in lookup)) {
              lookup[site] = 1;
              unique_sites.push(site);
            }
          }
        return unique_sites
    }

    // Map Layer Data
    //------------------------------------------------------------
    let map_data = daily_total_data.filter(filter_year_month);
    

    // Daily Total
    //------------------------------------------------------------
    // Prepare x and y data arrays
    let site_names = [];
    let total_counts = [];
    let total_data = [];

    for (d of map_data) {
        site_names.push(d['description']);
        total_counts.push(d[filter_day]);
    }

    //Append plotly.js compatible data
    total_data.push(
        {
            x: site_names,
            y: total_counts,
            type: 'bar',
            marker: {color: 'rgba(10, 10, 200, 0.5)'}
        }
    )

    // Daily Profile
    //------------------------------------------------------------
    // Prepare x and y data arrays
    let profile_data = daily_profile_data.filter(filter_year_month);
    let unique_sites = get_unique_sites(profile_data);

    let list_profile_data = [];

    for (site of unique_sites) {

        let hours = [];
        let profile_counts = [];
        
        for (d of profile_data) {
            if (d['site_id'] === site) {
                hours.push(d['hours']);
                profile_counts.push(d[filter_day]);
            }
        }

        //Append plotly.js compatible data
        list_profile_data.push(
            {
                x: hours,
                y: profile_counts,
                type: 'scatter',
                marker: {color: 'rgba(10, 10, 200, 0.5)'},
                mode: 'lines',
                line: {shape: 'spline'}
            }
        );
    }

    // Return datasets
    return {
        map_data: map_data, 
        total_data: total_data,
        profile_data: list_profile_data
    }

}
//------------------------------------------------------------------------------------------


//Update Data, Map Layers and Charts - OD Trips
function sync_update_od_trips(filter_route, filter_year_month, filter_day_time) {

    //Extract data based on the filters
    extract_od_trips(filter_route, filter_year_month, filter_day_time);
    
    //Manipulate the received data
    filtered_data = od_trips_data_manipulation(board_alight_count);

    //Update map layer data
    // Scale the width value
    let width_val = [];
    for (d of od_trips) {
        width_val.push(d.quantity);
    }
    let max_width_val = Math.max(...width_val);

    layer_od_trip_routes.setProps({data: od_trips_routes});
    layer_od_trip_stops.setProps({data: od_trips_stops});
    layer_od_trip.setProps({data: od_trips, getStrokeWidth: d => d.quantity / max_width_val * 30});
    
    //Update charts
    od_trips_charts_update(filtered_data);
}



//Process OD Trips
//------------------------------------------------------------------------------------------
function od_trips_data_manipulation(board_alight_data) {
    
    console.log('PROCESSED_DATA:', board_alight_data);

    //Create data for Plotly charts
    
    //Function to list unique trip_id - for getting pt directions
    function get_unique_direction(array_data) {

        let unique_dir = [];

        for (i of array_data) {
            if (unique_dir.indexOf(i.direction) > -1) {
                continue;
            } 
            else {
                unique_dir.push(i.direction);
            }
        }
        return unique_dir
    }

    //Prepare area chart dataset for boarding

    //Get unique trip_id
    list_dir = get_unique_direction(board_alight_data);

    //Split data by trip_id
    let board_alight_chart_data = [];

    for (dir_name of list_dir) { //by direction
        
        let board_stop_seq = [];
        let board_quan = [];

        let alight_stop_seq = [];
        let alight_quan = [];

        let stop_diagram_val = [];
        let stop_names = [];
        let stop_color_array = [];

        for (trip of board_alight_data) { //data per stop for the direction
            if (trip.direction === dir_name) { //only get the data with same direction

                if (trip.trip_type === 'boarding') { //for boarding data
                    board_stop_seq.push(trip.stop_sequence);
                    board_quan.push(trip.quantity);
                    stop_diagram_val.push(0);
                    stop_names.push(trip.stop_name);
                    stop_color_array.push('rgba(230, 230, 230, 0.9)');
                }
                else { //for alighting data
                    alight_stop_seq.push(trip.stop_sequence);
                    alight_quan.push(trip.quantity * -1); //Make the value negative for the chart
                }
            }
        }
        //Save array of x y datasets for boarding and alighting
        board_chart_data = {
            x: board_stop_seq,
            y: board_quan,
            type: 'scatter',
            mode: 'lines',
            line: { shape: 'spline', color: 'rgba(10, 10, 200, 1.0)' },
            fill: 'tozeroy',
            fillcolor: 'rgba(10, 10, 200, 0.3)',
            name: 'Boarding'
        };
        alight_chart_data = {
            x: alight_stop_seq,
            y: alight_quan,
            type: 'scatter',
            mode: 'lines',
            line: { shape: 'spline', color: 'rgba(200, 50, 30, 1.0)' },
            fill: 'tozeroy',
            fillcolor: 'rgba(200, 50, 30, 0.3)',
            name: 'Alighting'
        };
        stop_diagram_data = {
            x: board_stop_seq, //array of boarding stop_sequence to visualise stop diagram
            y: stop_diagram_val, //array of 0
            type: 'scatter',
            mode: 'lines+markers',
            line: { color: 'black', width: 1 },
            marker: { size: 8, color: stop_color_array, line: { width: 1, color: 'rgba(80, 80, 80, 1.0)' } },
            showlegend: false,
            name: 'PT Stop',
            text: stop_names,
            hovertext: stop_names,
            hoverinfo: 'text',
        }

        //Save the trip_id and the chart dataset to boarding data array
        board_alight_chart_data.push(
            {
                direction: dir_name,
                board_data: board_chart_data,
                alight_data: alight_chart_data,
                stop_data: stop_diagram_data
            }
        );
    }
    console.log('CHART DATA:', board_alight_chart_data);

    //Slider of stops for filtering ???



    return board_alight_chart_data
}