// Header Control
// Send AJAX Request to Flask
// Receive Data and Upload on Map and Charts


$(document).ready(function() {

    //Header - Bike-Count

    //Header Item - hover
    $("#header-item-bike").hover(
        function() {
            $(this).css('color','rgb(10, 10, 200)');
        }, 
        function() {
            $(this).css('color','rgb(80, 80, 80)');
        }
    );

    //Header Item - click
    $("#header-item-bike").click(function() {
        //Remove all existing layers
        remove_all_layers();
        //Remove all existing layer controls
        remove_all_layer_controls();
        //Remove all existing layer controls
        remove_all_charts();
        
        //Update background
        $(".header").css('background-color','rgba(10, 10, 200, 0.3)');

        //Send a simple POST request to get data:
        //bike_site_data
        //bike_count_total_data
        //bike_count_profile_data
        extract_bike_data();

        //Define initial data filtering
        let filter_year = 2019;
        let filter_month = 'June     ';
        let filter_day = 'monday';

        //Data manipulation
        let processed_data = bike_count_data_manipulation(
            filter_year, 
            filter_month, 
            filter_day, 
            bike_count_total_data, 
            bike_count_profile_data
            );

        //Create map layer
        bike_count_site_update(bike_site_data);
        bike_count_update(processed_data['map_data']);

        //Create charts
        bike_count_charts(
            processed_data['total_data'], 
            processed_data['profile_data']
            );

        //Add layer control, data control and charts
        $("#layers-bike").fadeIn();
        $(".control-layer button").css('background-color', 'rgba(10, 10, 200, 0.5)');
        $(".control-layer button:hover").css('background-color', 'rgba(10, 10, 200, 0.3)');
        $(".control-layer button:active").css('background-color', 'rgba(10, 10, 200, 0.3)');
        $("#data-bike").fadeIn();
        $(".control-data select").css('background-color', 'rgba(10, 10, 200, 0.5)');
        $("#chart-bike").fadeIn();
        
        // Add layer visibility functions
        $("#toggle-bike-sites-layer").click(function() {
            toggle_visibility('bike-count-site');
        });
        $("#toggle-bike-counts-layer").click(function() {
            toggle_visibility('bike-count');
        });
    });


    //Header - OD-Trip

    //Header Item - hover
    $("#header-item-odtrip").hover(
        function() {
            $(this).css('color','rgb(230, 130, 60)');
        }, 
        function() {
            $(this).css('color','rgb(80, 80, 80)');
        }
    );

    //Header Item - click
    $("#header-item-odtrip").click(function() {
        //Remove all existing layers
        remove_all_layers();
        //Remove all existing layer controls
        remove_all_layer_controls();
        //Remove all existing layer controls
        remove_all_charts();
        
        //Update background
        $(".header").css('background-color','rgba(230, 130, 60, 0.3)');

        //Send a simple POST request to get data:
        //od_trips
        //board_alight_count
        //od_trips_stopss
        //od_trips_routes
        extract_od_trips('200', '2019-04', 'Weekend');

        //Data manipulation
        let processed_data = od_trips_data_manipulation(board_alight_count);

        //Create map layer
        od_trip_routes_update(od_trips_routes);
        od_trip_stops_update(od_trips_stops);
        od_trip_card_update(od_trips);

        //Create charts
        od_trips_charts(processed_data);

        //Add layer control
        $("#layers-od-trips").fadeIn();
        $(".control-layer button").css('background-color', 'rgba(230, 130, 60, 0.5)');
        $(".control-layer button:hover").css('background-color', 'rgba(230, 130, 60, 0.3)');
        $(".control-layer button:active").css('background-color', 'rgba(230, 130, 60, 0.3)');
        $("#data-od-trips").fadeIn();
        $(".control-data select").css('background-color', 'rgba(230, 130, 60, 0.5)');
        $("#chart-od-trips").fadeIn();

        // Add layer visibility functions
        $("#toggle-od-trips-layer").click(function() {
            toggle_visibility('od-trips');
        })
        $("#toggle-stops-layer").click(function() {
            toggle_visibility('od-trips-stops');
        })
        $("#toggle-routes-layer").click(function() {
            toggle_visibility('od-trips-routes');
        })
    });


    //Header - PT

    //Header Item - hover
    $("#header-item-gtfs").hover(
        function() {
            $(this).css('color','rgb(215, 200, 15)');
        }, 
        function() {
            $(this).css('color','rgb(80, 80, 80)');
        }
    );
    //Header Item - click
    $("#header-item-gtfs").click(function() {
        //Remove all existing layers
        remove_all_layers();
        //Remove all existing layer controls
        remove_all_layer_controls();
        //Remove all existing layer controls
        remove_all_charts();
        
        //Update background
        $(".header").css('background-color','rgba(215, 200, 15, 0.3)');

        //Send a simple POST request to get data
        $.ajax({
            url: 'gtfs',
            data: {},
            datatype: 'JSON',
            type: 'POST',
            success: function(data) {
                gtfs_update(data);
            },
            error: function() {
                console.log("SORRY!! :'(");
            }
        })
    });


    //Header - Crash

    //Header Item - hover
    $("#header-item-crash").hover(
        function() {
            $(this).css('color','rgb(200, 50, 30)');
        }, 
        function() {
            $(this).css('color','rgb(80, 80, 80)');
        }
    );
    //Header Item - click
    $("#header-item-crash").click(function() {
        //Remove all existing layers
        remove_all_layers();
        //Remove all existing layer controls
        remove_all_layer_controls();
        //Remove all existing layer controls
        remove_all_charts();
        
        //Update background
        $(".header").css('background-color','rgba(200, 50, 30, 0.3)');

        //Send a simple POST request to get data
        $.ajax({
            url: 'crash',
            data: {},
            datatype: 'JSON',
            type: 'POST',
            success: function(data) {
                crash_update(data);
            },
            error: function() {
                console.log("SORRY!! :'(");
            }
        })

        //Add layer control
        $("#layers-crash").fadeIn();
        $(".control-layer button").css('background-color', 'rgba(200, 50, 30, 0.5)');
        $(".control-layer button:hover").css('background-color', 'rgba(200, 50, 30, 0.3)');
        $(".control-layer button:active").css('background-color', 'rgba(200, 50, 30, 0.3)');
        $("#data-crash").fadeIn();
        $(".control-data select").css('background-color', 'rgba(200, 50, 30, 0.5)');

        // Add layer visibility functions
        $("#toggle-crash-layer").click(function() {
            toggle_visibility('crash-data');
        })
    });


    //Header - Travel-Time

    //Header Item - hover
    $("#header-item-traveltime").hover(
        function() {
            $(this).css('color','rgb(20, 150, 20)');
        }, 
        function() {
            $(this).css('color','rgb(80, 80, 80)');
        }
    );
    //Header Item - click
    $("#header-item-traveltime").click(function() {
        //Remove all existing layers
        remove_all_layers();
        //Remove all existing layer controls
        remove_all_layer_controls();
        //Remove all existing layer controls
        remove_all_charts();

        //Update background
        $(".header").css('background-color','rgba(20, 150, 20, 0.3)');

        //Send a simple POST request to get data
        $.ajax({
            url: 'travel_time',
            data: {},
            datatype: 'JSON',
            type: 'POST',
            success: function(data) {
                travel_time_update(data);
            },
            error: function() {
                console.log("SORRY!! :'(");
            }
        })

        //Add layer control
        $("#layers-travel-times").fadeIn();
        $(".control-layer button").css('background-color', 'rgba(20, 150, 20, 0.5)');
        $(".control-layer button:hover").css('background-color', 'rgba(20, 150, 20, 0.3)');
        $(".control-layer button:active").css('background-color', 'rgba(20, 150, 20, 0.3)');
        $("#data-travel-times").fadeIn();
        $(".control-data select").css('background-color', 'rgba(20, 150, 20, 0.5)');

        // Add layer visibility functions
        $("#toggle-travel-times-layer").click(function() {
            toggle_visibility('bluetooth-links');
        })
    });

})


// Function to remove all layers from the map
function remove_all_layers() {
    let layers = [
        'bike-count-site','bike-count',
        'od-trips-routes','od-trips-stops','od-trips',
        'gtfs-data',
        'crash-data',
        'bluetooth-links'
    ]
    for (l of layers) {
        if (map.getLayer(l)) map.removeLayer(l);
    }
}


// Function to remove all layer controls
function remove_all_layer_controls() {
    let layer_controls = [
        'layers-bike', 'data-bike',
        'layers-od-trips', 'data-od-trips',
        'layers-gtfs', 'data-gtfs',
        'layers-crash', 'data-crash',
        'layers-travel-times', 'data-travel-times'
    ]
    for (l of layer_controls) {
        if (document.getElementById(l) !== null) {
            let id_l = '#' + l
            $(id_l).fadeOut();
        }
    }
}


// Function to remove all charts
function remove_all_charts() {
    let charts = [
        'chart-bike', 
        'chart-od-trips',
        'chart-gtfs', 
        'chart-crash', 
        'chart-travel-times'
    ]
    for (c of charts) {
        if (document.getElementById(c) !== null) {
            let id_c = '#' + c
            $(id_c).fadeOut();
        }
    }
}