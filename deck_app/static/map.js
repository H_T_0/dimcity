const {MapboxLayer} = deck;

// Get a mapbox API access token
mapboxgl.accessToken = mapbox_token


// Initialize mapbox map
const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/htak10/ck1kfkp5d0e6c1cs88hw0acp3',
  center: [153.025858, -27.471302],
  zoom: 10,
  bearing: 0,
  pitch: 35
});
