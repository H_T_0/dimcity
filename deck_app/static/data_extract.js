// Function to call ajax request to extract datasets from database 

//Bike Counts
function extract_bike_data() {
    $.ajax({
        url: 'bike_counts',
        data: {},
        datatype: 'JSON',
        type: 'POST',
        async: false,
        success: function(data) {

            // keep the data in-memory
            bike_site_data = data[0];
            bike_count_total_data = data[1];
            bike_count_profile_data = data[2];

        },
        error: function() {
            console.log("SORRY!! :'(");
        }
    })
}


//OD Trips
function extract_od_trips(route, year_month, day_time) {

    console.log(route, year_month, day_time)

    let post_data = { 
        'route': route, 
        'year_month': year_month, 
        'day_time': day_time 
    }

    $.ajax({
        url: 'od_trips',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(post_data),
        datatype: 'json',
        type: 'POST',
        async: false,
        success: function(data) {

            // keep the data in-memory
            console.log('OD_TRIPS', data[0])
            od_trips = data[0];
            board_alight_count = data[1];
            od_trips_stops = data[2];
            od_trips_routes = data[3];

        },
        error: function() {
            console.log("SORRY!! :'(");
        }
    });    
}
