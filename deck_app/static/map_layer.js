// Functions - create and upload map layers

function bike_count_site_update(bike_count_site_data) {
    console.log("BIKE COUNT SITE DATA: ", bike_count_site_data);

    // Creation of layer
    const bike_count_site_layer = new MapboxLayer({
        id: 'bike-count-site',
        type: ScatterplotLayer,
        data: bike_count_site_data,
        visible: true,
        filled: true,
        radiusScale: 1,
        radiusMinPixels: 1,
        radiusMaxPixels: 2,
        opacity: 0.8,
        getPosition: d => d.coordinates,
        getRadius: d => 20,
        getFillColor: d => [10, 10, 200],
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });

    // Add bikeCountSiteLayer to map
    map.addLayer(bike_count_site_layer);
}


function bike_count_update(bike_count_data) {
    console.log("BIKE COUNT DATA: ", bike_count_data);

    // Creation of layer
    const bike_count_layer = new MapboxLayer({
        id: 'bike-count',
        type: ScatterplotLayer,
        data: bike_count_data,
        visible: true,
        filled: true,
        radiusScale: 1,
        radiusMinPixels: 3,
        radiusMaxPixels: 50,
        opacity: 0.2,
        getPosition: d => d.coordinates,
        getRadius: d => d.monday,
        getFillColor: d => [10, 10, 200],
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });

    // Add bikeCountSiteLayer to map
    map.addLayer(bike_count_layer);

    // Save the layer so it can be updated by user control
    layer_bike_count = bike_count_layer;
}


function od_trip_routes_update(od_trip_routes_data) {
    console.log("OD TRIP ROUTES DATA: ", od_trip_routes_data);

    // Createion of layer
    const od_trip_routes_layer = new MapboxLayer({
        id: 'od-trips-routes',
        type: PathLayer,
        data: od_trip_routes_data,
        visible: true,
        filled: true,
        widthScale: 1,
        widthMinPixels: 5,
        opacity: 0.8,
        getPath: d => d.path,
        getColor: d => [100, 100, 100],
        getWidth: d => 5,
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });
    
    // Add bikeCountSiteLayer to map
    map.addLayer(od_trip_routes_layer);
    
    // Save the layer so it can be updated by user control
    layer_od_trip_routes = od_trip_routes_layer;
}


function od_trip_stops_update(od_trip_stops_data) {
    console.log("OD TRIP STOPS DATA: ", od_trip_stops_data);

    // Createion of layer
    const od_trip_stops_layer = new MapboxLayer({
        id: 'od-trips-stops',
        type: ScatterplotLayer,
        data: od_trip_stops_data,
        visible: true,
        filled: true,
        stroked: true,
        radiusScale: 1,
        radiusMinPixels: 3,
        radiusMaxPixels: 10,
        lineWidthMinPixels: 1,
        opacity: 0.8,
        getPosition: d => d.coordinates,
        getRadius: d => 10,
        getFillColor: d => [230, 230, 230],
        getLineColor: d => [50, 50, 200],
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });

    // Add bikeCountSiteLayer to map
    map.addLayer(od_trip_stops_layer);
    
    // Save the layer so it can be updated by user control
    layer_od_trip_stops = od_trip_stops_layer;
}


function od_trip_card_update(od_trips_data) {
    console.log("OD TRIP DATA: ", od_trips_data);

    // Scale the width value
    let width_val = [];
    for (d of od_trips_data) {
        width_val.push(d.quantity);
    }
    let max_width_val = Math.max(...width_val);

    // Creation of layer
    const od_trip_layer = new MapboxLayer({
        id: 'od-trips',
        type: ArcLayer,
        data: od_trips_data,
        visible: true,
        widthUnits: 'pixels',
        widthScale: 1,
        widthMinPixels: 0,
        widthMinPixels: 30,
        opacity: 0.1,
        getSourcePosition: d => d.from.coordinates,
        getTargetPosition: d => d.to.coordinates,
        getSourceColor: d => [10, 10, 200], //Boarding stop
        getTargetColor: d => [200, 50, 30], //Alighting stop
        getStrokeWidth: d => d.quantity / max_width_val * 30,
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });
    
    // Add bikeCountSiteLayer to map
    map.addLayer(od_trip_layer);
    
    // Save the layer so it can be updated by user control
    layer_od_trip = od_trip_layer;
}


function gtfs_update(gtfs_data) {
    console.log("PT DATA: ", gtfs_data);

    // Creation of layer
    const gtfs_layer = new MapboxLayer({
        id: 'gtfs-data',
        type: ScatterplotLayer,
        data: gtfs_data,
        visible: true,
        filled: true,
        radiusScale: 5,
        radiusMinPixels: 3,
        radiusMaxPixels: 30,
        opacity: 0.6,
        getPosition: d => d.coordinates,
        getRadius: d => 20,
        getFillColor: d => [10, 10, 200],
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });

    // Add bikeCountSiteLayer to map
    map.addLayer(gtfs_layer);
}


function crash_update(crash_data) {
    console.log("CRASH DATA: ", crash_data);

    // Creation of layer
    const crash_layer = new MapboxLayer({
        id: 'crash-data',
        type: HexagonLayer,
        data: crash_data,
        visible: true,
        extruded: true,
        radius: 200,
        elevationScale: 10,
        opacity: 0.6,
        getPosition: d => d.COORDINATES,
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240]
    });
    
    // Add bikeCountSiteLayer to map
    map.addLayer(crash_layer);
}


function travel_time_update(travel_time_data) {
    console.log("TRAVEL TIME DATA: ", travel_time_data);

    // Creation of layer
    const traveltime_site_layer = new MapboxLayer({
        id: 'bluetooth-links',
        type: LineLayer,
        data: travel_time_data,
        visible: true,
        opacity: 0.8,
        getWidth: 50,
        getSourcePosition: d => d.from.coordinates,
        getTargetPosition: d => d.to.coordinates,
        getColor: d => [20, 255 * d.travel_time / Math.max(d.travel_time), 20],
        pickable: true,
        autoHighlight: true,
        highlightColor: [250, 70, 240],
        //onHover: info => setTooltip(info.object, info.x, info.y), // hover info
    });

    // Add bikeCountSiteLayer to map
    map.addLayer(traveltime_site_layer);
}