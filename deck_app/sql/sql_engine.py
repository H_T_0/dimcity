"""
sql_engine.py stores SQL query strings for different datasets into a dictionary.
SQL query strings can be called in other scripts to execute data manipulation.

"""

########## SQL Queries for OD Trips data ##########
sql_od_trip_sites = """ 
                    SELECT DISTINCT 
                        origin_stop AS stop_id,
                        stop_lon,
                        stop_lat
                    FROM (
                        SELECT DISTINCT origin_stop FROM od_trip.od_trips 
                        UNION 
                        SELECT DISTINCT destination_stop FROM od_trip.od_trips
                    ) AS od_stop 
                    INNER JOIN gtfs.stops ON od_stop.origin_stop = stop_id 
                    WHERE (
                        NOT origin_stop LIKE '\%%NULL\%%' AND 
                        NOT origin_stop LIKE '\%%n/a\%%'
                        )
                    ORDER BY origin_stop
                    """

sql_od_trip_summary = """
                    SELECT time, ticket_type, month, SUM(quantity)
                    FROM od_trip.od_trips 
                    GROUP BY month, time, ticket_type
                    ORDER BY time, ticket_type
                    """

sql_od_trip_summary_by_route = """
                            SELECT time, ticket_type, route, month, SUM(quantity)
                            FROM od_trip.od_trips 
                            GROUP BY month, time, ticket_type, route
                            ORDER BY time, ticket_type
                            """

sql_od_trip_summary_by_OD = """
                            SELECT time, ticket_type, origin_stop, destination_stop, month, SUM(quantity)
                            FROM od_trip.od_trips 
                            GROUP BY month, time, ticket_type, origin_stop, destination_stop
                            ORDER BY time, ticket_type
                            """

sql_od_trip_summary_by_origin = """
                                SELECT time, ticket_type, origin_stop, month, SUM(quantity)
                                FROM od_trip.od_trips 
                                GROUP BY month, time, ticket_type, origin_stop
                                ORDER BY time, ticket_type
                                """

sql_od_trip_summary_by_destination = """
                                    SELECT time, ticket_type, destination_stop, month, SUM(quantity)
                                    FROM od_trip.od_trips 
                                    GROUP BY month, time, ticket_type, destination_stop
                                    ORDER BY time, ticket_type
                                    """

sql_od_trip_paper_total = """
                            SELECT 
                                --id,
                                --operator
                                od.month,
                                od.route,
                                od.direction,
                                --od.time,
                                --ticket_type,
                                od.origin_stop,
                                stop.stop_name AS origin_stop_name,
                                stop.zone_id AS origin_zone_id,
                                SUM(od.quantity) AS quantity,
                                stop.stop_lat AS origin_lat,
                                stop.stop_lon AS origin_lon
                            FROM od_trip.od_trips od
                            INNER JOIN gtfs.stops stop ON od.origin_stop = stop.stop_id
                            WHERE ticket_type='Paper'
                            GROUP BY 
                                od.month,
                                od.route,
                                od.direction,
                                od.origin_stop,
                                stop.stop_name,
                                stop.zone_id,
                                stop.stop_lat,
                                stop.stop_lon
                            """

sql_od_trip_paper = """
                    SELECT 
                        --id,
                        --operator
                        od.month,
                        od.route,
                        od.direction,
                        od.time,
                        --ticket_type,
                        od.origin_stop,
                        stop.stop_name AS origin_stop_name,
                        stop.zone_id AS origin_zone_id,
                        od.destination_stop,
                        od.quantity,
                        stop.stop_lat AS origin_lat,
                        stop.stop_lon AS origin_lon
                    FROM od_trip.od_trips od
                    INNER JOIN gtfs.stops stop ON od.origin_stop = stop.stop_id
                    WHERE ticket_type='Paper'
                    """

sql_od_trip_card_total = """
                        SELECT 
                            --id,
                            --operator
                            od.route,
                            od.month,
                            --od.direction,
                            --od.time,
                            --ticket_type,
                            od.origin_stop,
                            --stop_o.stop_name AS origin_stop_name,
                            --stop_o.zone_id AS origin_zone_id,
                            od.destination_stop,
                            --stop_d.stop_name AS destination_stop_name,
                            --stop_d.zone_id AS destination_zone_id,
                            SUM(od.quantity) AS quantity,
                            stop_o.stop_lat AS origin_lat,
                            stop_o.stop_lon AS origin_lon,
                            stop_d.stop_lat AS destination_lat,
                            stop_d.stop_lon AS destination_lon
                        FROM od_trip.od_trips od
                        INNER JOIN gtfs.stops stop_o ON od.origin_stop = stop_o.stop_id
                        INNER JOIN gtfs.stops stop_d ON od.destination_stop = stop_d.stop_id
                        WHERE ticket_type='go card' AND route='140' 
                        GROUP BY 
                            od.route,
                            od.month,
                            od.origin_stop,
                            od.destination_stop,
                            stop_o.stop_lat,
                            stop_o.stop_lon,
                            stop_d.stop_lat,
                            stop_d.stop_lon
                            """

sql_od_trip_card = """
                    SELECT 
                        --id,
                        --operator
                        od.month,
                        od.route,
                        od.direction,
                        od.time,
                        --ticket_type,
                        od.origin_stop,
                        stop_o.stop_name AS origin_stop_name,
                        stop_o.zone_id AS origin_zone_id,
                        od.destination_stop,
                        stop_d.stop_name AS destination_stop_name,
                        stop_d.zone_id AS destination_zone_id,
                        od.quantity,
                        stop_o.stop_lat AS origin_lat,
                        stop_o.stop_lon AS origin_lon,
                        stop_d.stop_lat AS destination_lat,
                        stop_d.stop_lon AS destination_lon
                    FROM od_trip.od_trips od
                    INNER JOIN gtfs.stops stop_o ON od.origin_stop = stop_o.stop_id
                    INNER JOIN gtfs.stops stop_d ON od.destination_stop = stop_d.stop_id
                    WHERE ticket_type='go card'
                    """


######### SQL Queries for GTFS data ##########


######### SQL Queries for Crash data ##########
sql_crash_locations = """
                        SELECT 
                            crash_ref_number,
                            crash_severity,
                            crash_year,
                            crash_month,
                            crash_day_of_week,
                            crash_hour,
                            crash_nature,
                            crash_type,
                            crash_longitude_gda94,
                            crash_latitude_gda94 
                        FROM crash.locations
                        LIMIT 10000
                        """


######### SQL Queries for Bluetooth data ##########
sql_bluetooth_links = """
                    SELECT * FROM bluetooth_traveltime.links
                    """

sql_bluetooth_traveltimes = """
                            SELECT * FROM bluetooth_traveltime.travel_times 
                            WHERE interval_end = '1/10/2019 0:00'
                            """


######### SQL Queries for Bike Count data ##########
sql_bike_sites = """
                SELECT * FROM bike_counts.count_site
                """

sql_bike_counts_daily_total = """
                                SELECT 
                                    counts.site_id,
                                    counts.description,
                                    counts.months,
                                    counts.years,
                                    SUM(counts.saturday) AS saturday,
                                    SUM(counts.sunday) AS sunday,
                                    SUM(counts.monday) AS monday,
                                    SUM(counts.tuesday) AS tuesday,
                                    SUM(counts.wednesday) AS wednesday,
                                    SUM(counts.thursday) AS thursday,
                                    SUM(counts.friday) AS friday,
                                    sites.latitude,
                                    sites.longitude 
                                FROM bike_counts.bike_count counts 
                                INNER JOIN bike_counts.count_site sites ON counts.site_id = sites.site_id
                                GROUP BY counts.site_id, counts.description, counts.years, counts.months, sites.latitude, sites.longitude
                                """

sql_bike_counts_daily_profile = """
                                SELECT 
                                    counts.site_id,
                                    counts.description,
                                    counts.hours,
                                    counts.months,
                                    counts.years,
                                    counts.saturday,
                                    counts.sunday,
                                    counts.monday,
                                    counts.tuesday,
                                    counts.wednesday,
                                    counts.thursday,
                                    counts.friday,
                                    sites.latitude,
                                    sites.longitude 
                                FROM bike_counts.bike_count counts 
                                INNER JOIN bike_counts.count_site sites ON counts.site_id = sites.site_id
                                """

######### Dictionary of SQL Strings #########
sql_dict = {
    # OD Trips
    'sql_od_trip_sites': sql_od_trip_sites,
    'sql_od_trip_summary': sql_od_trip_summary,
    'sql_od_trip_summary_by_route': sql_od_trip_summary_by_route,
    'sql_od_trip_summary_by_OD': sql_od_trip_summary_by_OD,
    'sql_od_trip_summary_by_origin': sql_od_trip_summary_by_origin,
    'sql_od_trip_summary_by_destination': sql_od_trip_summary_by_destination,
    'sql_od_trip_paper_total': sql_od_trip_paper_total,
    'sql_od_trip_paper': sql_od_trip_paper,
    'sql_od_trip_card_total': sql_od_trip_card_total,
    'sql_od_trip_card': sql_od_trip_card,
    # Crash
    'sql_crash_locations': sql_crash_locations,
    # Bike Couunts
    'sql_bike_sites': sql_bike_sites,
    'sql_bike_counts_daily_total': sql_bike_counts_daily_total,
    'sql_bike_counts_daily_profile': sql_bike_counts_daily_profile,
    # Bluetooth Travel Time
    'sql_bluetooth_links': sql_bluetooth_links,
    'sql_bluetooth_traveltimes': sql_bluetooth_traveltimes
    }