ALTER SEQUENCE crash.driver_demographics_id_seq RESTART WITH 1;
ALTER SEQUENCE crash.factors_in_road_crashes_id_seq RESTART WITH 1;
ALTER SEQUENCE crash.locations_id_seq RESTART WITH 1;
ALTER SEQUENCE crash.restraint_helmet_use_id_seq RESTART WITH 1;
ALTER SEQUENCE crash.vehicle_involvement_id_seq RESTART WITH 1;