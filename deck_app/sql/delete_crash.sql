DELETE FROM crash.driver_demographics;
DELETE FROM crash.factors_in_road_crashes;
DELETE FROM crash.locations;
DELETE FROM crash.restraint_helmet_use;
DELETE FROM crash.vehicle_involvement;