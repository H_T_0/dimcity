from flask import Flask, jsonify, request, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import yaml


# Create flask app
app = Flask(__name__, template_folder='./templates')


# Set up database environemnt variables
with open('../config.yaml', 'r') as params:

    # read yam file
    yaml_params = yaml.safe_load(params)
    
    # load parameter values
    db_uri = yaml_params['DATABASE']['DATABASE_URI']
    db_track_mod = yaml_params['DATABASE']['TRACK_MODIFICATIONS']
    db_eng_opt = yaml_params['DATABASE']['ENGINE_OPTIONS']
    mapbox_token = yaml_params['MAPBOX']['API_KEY']

    # Set up app's environment variables
    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = db_track_mod


# Initialize database object, database engine and migration
db = SQLAlchemy(app)
engine = db.create_engine(db_uri, engine_opts=db_eng_opt)
migrate = Migrate(app, db=db)