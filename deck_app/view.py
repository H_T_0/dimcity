from app import app, mapbox_token
from flask import render_template, request, jsonify
import json

from sql.sql_engine import sql_dict
from engine import *


# Initial Page
# --------------------------------------------------------------------------------
@app.route('/')
def index():

    # save mapbox api token in a dict
    mapbox_key = {'mapbox_token': mapbox_token}

    # initial layer data
    layer_data = {'data': {'name': 'init', 'coordinates': [0, 0]}}

    return render_template('index.html', mapbox_key=mapbox_key, layer_data=layer_data)
# --------------------------------------------------------------------------------


# Data Loading Pages
# --------------------------------------------------------------------------------
# Bike Counts
@app.route('/bike_counts', methods=['POST'])
def bike_counts():
    if request.method == 'POST':
        # Obtain data required
        bike_count_site_data = create_layer_scatter(sql_dict['sql_bike_sites'], ['longitude', 'latitude'])
        bike_count_daily_total_data = create_layer_scatter(sql_dict['sql_bike_counts_daily_total'], ['longitude', 'latitude'])
        bike_count_daily_profile_data = create_layer_scatter(sql_dict['sql_bike_counts_daily_profile'], ['longitude', 'latitude'])

        # Return the data as json
        return jsonify(
            bike_count_site_data, 
            bike_count_daily_total_data, 
            bike_count_daily_profile_data
            )

    return jsonify([])

# OD Trips
@app.route('/od_trips', methods=['POST'])
def od_trips():
    if request.method == 'POST':
        print('FLASK RECEIVED DATA: ', request.get_json())
        route = request.get_json()['route']
        year_month = request.get_json()['year_month']
        day_time = request.get_json()['day_time']

        # Obtain data required
        od_trip_data, board_alight_data, stops_data, shapes_data = create_layer_od_trips(route, year_month, day_time)

        # Return the data as json
        return jsonify(od_trip_data, board_alight_data, stops_data, shapes_data)

    return jsonify([])

# GTFS
@app.route('/gtfs', methods=['POST'])
def gtfs():
    if request.method == 'POST':
        # Obtain data required
        gtfs_data = {'TBC': 'TBC'}

        # Return the data as json
        return jsonify(gtfs_data)

    return jsonify([])

# Crash
@app.route('/crash', methods=['POST'])
def crash():
    if request.method == 'POST':
        # Obtain data required
        crash_data = create_layer_crash(sql_dict['sql_crash_locations'], ['crash_longitude_gda94', 'crash_latitude_gda94'])

        # Return the data as json
        return jsonify(crash_data)

    return jsonify([])

# Travel Time
@app.route('/travel_time', methods=['POST'])
def travel_time():
    if request.method == 'POST':
        
        # create travel time layer data
        traveltime_data = create_layer_traveltime(
            sql_dict['sql_bluetooth_links'], 
            sql_dict['sql_bluetooth_traveltimes'], 
            ['origin_longitude','origin_latitude'], 
            ['dest_longitude','dest_latitude']
            )

        # Return the data as json
        return jsonify(traveltime_data)

    return jsonify([])
# --------------------------------------------------------------------------------


# Run Application
# --------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run(debug=True, host='localhost', port=8070)
# --------------------------------------------------------------------------------