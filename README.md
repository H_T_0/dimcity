# dimcity

Open data visualisation and analytics project


## Project Set Up

1. create a python virtual environment with requirements.txt

2. create config.yaml to specify database details and api key

3. create a database

4. prepare datasets
    - unzip data.7z in data directory (get rid of 'data' folder)
    - update local data file paths in ./deck_app/sql/copy_*.sql files
    - depends on what database is used, sql may require changes (i.e. ALTER SEQUENCE)

5. initialize database schema and load static data
    - python
    - from models import db, engine, init_db, init_data
    - init_db() (creating table schema)
    - init_data() (delete existing data, set sequence and insert data)
