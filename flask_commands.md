## Run application
flask run

## Run python with flask application imports
flask shell

## Database migration

- initialize a migration repository (with specified db config)
flask db init

- create migration files
flask db migrate -m "tablename table"
i.e. flask db migrate -m "user table" will set a table called user in db (table not created yet)

- commit migration
flask db upgrade